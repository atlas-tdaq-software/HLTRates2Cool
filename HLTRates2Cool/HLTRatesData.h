#ifndef HLTRATES2COOL_HLTRATESDATA_H_
#define HLTRATES2COOL_HLTRATESDATA_H_

#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>

#include "is/info.h"
#include "is/infoT.h"
#include "is/inforeceiver.h"
#include "is/infodynany.h"
#include "TRP/TimePoint_IS.h"
#include "oh/core/HistogramData.h"

namespace hlt2cool {
/*! \brief A class that stores the information of the IS object that HLT
  *         rates are retrieved from. It's members are filled after the
  *         first time one the update() methods is invoked.
*
* One can input either Rate20 histogram or the ISS_TRP.HLT_Rate object.
* The update method will fill data members such as m_rates with the data.
* The class defines +, * and / operators for averaging over the class instances
* with weights. It's designed to be used as follows. Note that weights only
* affect the rates, and not number of providers or publication time.
*
*     std::vector<HLTRatesData> v_data(2);
*     for (auto a: v_data) v_data.Update(...);
*     HLTRatesData sum = v_data[0] * weight0 + v_data[1] * weight1;
*     sum = sum / 2;
*
* Weights should be calculated from the relative number of providers between instances
*/

class HLTRatesData {
 private:
  /// Reordered rate vector, independent of the IS object.
  /// Format is same as the ISS_TRP.HLT_Rate data member.
  /// \sa update()
  std::vector<float> m_rates;

  /// Chain names, set by update method on the first call
  std::vector<std::string> m_chains;

  /// Number of rate items
  unsigned m_nrateitems = 7;

  /// Rate types(input, prescale, raw...) set by update method on the first call
  std::vector<std::string> m_rateitems;

  /// Number of providers in each level (from histogram annotations).
  /// This data is in the m_rates, but kept also seperate for convenience
  std::vector<int> m_providers;

  /// Set to false first time update() is invoked
  bool m_first = 1;

  /// Unix timestamp that histogram was published.
  uint64_t m_pub_time;

  /// Version number for the data. It should be incremented by one at every change.
  /// It doesn't change the internal representation of the data in the class, but
  /// only output of getRates() and getChains() methods.
  float m_dataversion = 2.0;

  /// Hash table between chain names and chain IDs
  std::unordered_map<std::string, int> m_chain_map;

 public:
  /// Default Constructors.
  HLTRatesData() {}
  ~HLTRatesData() {}

  /// Constructor that updates the data immediately
  explicit HLTRatesData(ISCallbackInfo* isc);

  /// Updates the class data with new IS object, it checks the object type
  /// to decide which of update methods to use
  void update(ISCallbackInfo* isc);

  /// Updates the class data with HistogramData<float>
  void update(const oh::HistogramData<float> & hd);

  /// Updates the class data with TimePoint_IS object
  void update(const TimePoint_IS & tp);

  /// Update with random data, useful to test cool writing
  ///
  /// \param nchains Number of chains to create data for
  /// \sa update()
  void updateSim(unsigned nchains = 1000);

  /// Functions to print rates of a certain or all chains, used for debugging
  /// Not necessary in the final release
  // void PrintChainRates(std::string chainname);
  // void PrintChainRates(int chainid);

  /// Create a hash table between chain names and id's
  /// Should be run only in first trial
  void createChainMap();

  /// Resets all elements of member vectors to 0.
  void reset();

  /// Returns the rate data, as it will be published to COOL
  std::vector<float> getRates();

  /// Returns the chain names, as it will be published to COOL
  std::vector<std::string> getChains();

  int getPublicationTime() {return m_pub_time;}

  std::vector<int> getProviders() {return m_providers;}

  /// sum operator for class, sums the m_rates,providers, publication time
  friend HLTRatesData operator+(HLTRatesData lhs, const HLTRatesData& rhs);

  //+= is not friend
  HLTRatesData& operator+=(const HLTRatesData& rhs)
  { *this = *this + rhs; return *this;}

  /// multiplication operator, only multiplies the rate related part of m_rates
  /// as it's only meant to be used for setting weight for each data point
  friend HLTRatesData operator*(const float weight, HLTRatesData data);

  /// \sa operator*()
  friend HLTRatesData operator*(HLTRatesData data, const float weight)
  {return weight * data;}

  /// division operator, divides m_rates,providers, publication time.
  /// It is used to average over sum of few instances
  friend HLTRatesData operator/(HLTRatesData lhs, int N);

  // Copy assignment operator
  // HLTRatesData & operator=(const HLTRatesData & d);

  friend std::ostream & operator<<(std::ostream & os, const HLTRatesData & c);
};

std::ostream & operator<<(std::ostream & os, const HLTRatesData & c);
}  // namespace hlt2cool
#endif  // HLTRATES2COOL_HLTRATESDATA_H_
