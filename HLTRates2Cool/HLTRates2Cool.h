#ifndef HLTRATES2COOL_HLTRATES2COOL_H_
#define HLTRATES2COOL_HLTRATES2COOL_H_

#include <string>
#include <iostream>
#include <memory>
#include <queue>
#include <tuple>
#include <vector>

#include "ipc/partition.h"
#include "is/inforeceiver.h"
#include "is/infodynany.h"
#include "is/infodictionary.h"
#include "TTCInfo/LumiBlock.h"
#include "rc/Ready4PhysicsInfo.h"
#include "rc/RunParams.h"

#include "HLTRates2Cool/HLTRatesData.h"
#include "HLTRates2Cool/HLTRatesCoolUtils.h"

ERS_DECLARE_ISSUE(hlt2cool,
                  HLTRates2Cool_Issue,
                  "HLTRates2Cool issue: " << message,
                  ((const char *)message)
                  )



namespace hlt2cool {

/*! \brief HLTRates2Cool Interface
*
* This class is an interface between the IS repository object and the COOL
* database. It is used by the main application that runs in the partition.
*
* It uses HLTRatesData to get the information of the IS object, and
* HLTRatesCoolUtils to do the COOL reading/writing. Main work is done in
* its callback() method
*
*/
class HLTRates2Cool {
 private:
  IPCPartition m_partition;

  /// Name of the IS object to subscribe to
  std::string m_isobject;

  /// ISInfoReceiver used to subscribe to IS objects
  std::unique_ptr<ISInfoReceiver> m_rec;

  /// HLTRatesData instance to calculate weighted average of instances
  HLTRatesData m_sum_data;

  /// HLTRatesData current instance
  HLTRatesData m_current_data;

  /// Initial number of providers, it's set in the beginning and used as a scale factor
  int m_nprov;

  /// Mutex to prevent a second callback from starting to write
  std::mutex m_mutex;

  /// Number of summed instances, it is reset at every cool write.
  int m_nsummed = 0;

  /// Pointer to HLTRatesCoolUtils
  std::unique_ptr<HLTRatesCoolUtils> m_cool;

  /// Is Info dictionary instance to get RunParams
  ISInfoDictionary m_isInfoDict;

  /// Run Number
  unsigned m_runno;

  /// Lumiblock
  unsigned m_lb;

  /// True if its writing rates into COOL for the first time
  bool m_first = 1;

  /// Minimum interval between publications to COOL(seconds)
  unsigned m_pubinterval;

  /// Number of total publications to be done to COOL, -1 means no maximum, publications will be done the whole run
  int m_maxpubs;

  /// Number of publications already done to COOL
  int m_npubs = 0;

  /// Unix Timestamp of last COOL publication(beginning of IOV)
  uint64_t m_coolpubtime;

  /// True if subscription is already done
  bool m_subscribed;

  /// True if app is running in lab mode.
  /// In this case the stable beams check is replaced with a check to see if LB>2
  bool m_labmode;

  /// Previous status for stable beams/ready4physics
  bool m_stable = 0;

  /// True if chain names are already written to COOL
  bool m_chainsWrittenToCool = false;

  /// Start time of the application, used to ensure first IOV doesn't start before app.  
  uint64_t m_app_start_time;

  /// queue of tuple<data, since, until>, used for buffering data in case COOL is not available
  std::queue<std::tuple<std::vector<float>, uint64_t, uint64_t>> dataqueue;

  /// Used in case COOL is still not accessible in the end of the run
  std::string fallback_sqlite_connection;

  /// Large enough to cover 5 hours, archiving data every 10 seconds
  size_t m_maximum_buffer_size = 3000;

  /// Write all info in the dataqueue into COOL, if fails, returns false
  /// If terminate signal received, or buffer size > m_maximum_buffer_size
  /// write into an sqlite file
  bool writeBuffer2Cool();

  /// Set the m_runno and m_lb
  void getRunLb();

  /// Sets m_stable=true if we reached ready4physics
  bool getReady4Physics();

  /// Return ready4physics is info update time
  uint64_t getReady4PhysicsTime();

  /// Return start of run time
  uint64_t getSORTime();

 public:
  /// Constructor
  ///
  /// \param partition Reference to IPCPartition from the main app
  /// \param isobject see: #m_isobject
  /// \param dbId Database connection string
  /// \param ratesfolderName Name of the cool folder for rates
  /// \param chainsfolderName Name of the cool folder for chain names
  /// \param ratestag Rates folder cool tag
  /// \param chainstag Chains folder cool tag
  /// \param pubint see: #m_pubinterval
  /// \param maxpubs see: #m_maxpubs
  HLTRates2Cool(const IPCPartition & partition,
                const std::string isobject, const std::string dbcon,
                const std::string ratesfolder, const std::string chainsfolder,
                const std::string ratestag, const std::string chainstag,
                const unsigned pubint, const int maxpubs, const bool labmode);

  /// Subscribe to IS repository
  void subscribe();

  /// Unsubscribe from IS repository
  void unsubscribe();

  /// Callback method, that writes the data gathered from IS into COOL
  ///
  /// This method does most of the work. With and IS callback, it gets the IS
  /// object that one subscribed to, and writes to COOL in the defined interval.
  /// Inside the method, stable beams flag and run-lumiblock information is
  /// checked. Once the stable beams are reached, a publication is done every
  /// #m_pubinterval seconds, up to #m_maxpubs publications.
  void callback(ISCallbackInfo * i);

  /// This method should be called after subscribe and it ensures that
  /// application doesn't quit before COOL writing ends or run stops.
  void run();

  /// Used with signal handler
  static bool m_signalreceived;

  /// Signal handler. This is not an ideal implementation as it modifies a static var.
  /// It will work in the current case, where there is only once instance of the class
  static void signalHandler(int signum);


  ~HLTRates2Cool();
};

bool HLTRates2Cool::m_signalreceived = 0;

}  // namespace hlt2cool
#endif  // HLTRATES2COOL_HLTRATES2COOL_H_

