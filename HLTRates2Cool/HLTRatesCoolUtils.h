#ifndef HLTRATES2COOL_HLTRATESCOOLUTILS_H_
#define HLTRATES2COOL_HLTRATESCOOLUTILS_H_

#include <iostream>
#include <string>
#include <vector>

#include "HLTRates2Cool/HLTRatesData.h"

#include "ers/ers.h"

#include "CoolKernel/DatabaseId.h"
#include "CoolKernel/IDatabaseSvc.h"
#include "CoolKernel/IDatabase.h"
#include "CoolKernel/Record.h"
#include "CoolKernel/RecordSpecification.h"
#include "CoolKernel/FolderSpecification.h"
#include "CoolApplication/Application.h"
#include "CoolKernel/ValidityKey.h"
#include "CoolKernel/IFolder.h"
#include "CoolKernel/Exception.h"
#include "CoolKernel/IObject.h"
#include "CoolKernel/IObjectIterator.h"
#include "CoolKernel/pointers.h"
#include "CoralKernel/Context.h"
#include "RelationalAccess/ConnectionServiceException.h"

namespace hlt2cool {


/*! \brief A class that takes care of cool access and I/O
  *
  * It is capable of writing into two different database folders.
  * Chain name folder holds the names of chains as as they appear in the
  * ISS_TRP. The application should write to this database
  * only once as the chain names don't change during the run.
  *
  * Rates folder hold the information of each chain for some or all rate variables:
  * input, prescaled, raw, output, rerun, algoIn, passedrerun
  *
  */

class HLTRatesCoolUtils {
 private:
  std::string m_dbId;  ///< Database connection string
  cool::IDatabasePtr m_dbPtr;  ///< Pointer to database

  cool::IFolderPtr m_ratesfolder;  ///< Pointer to cool folder of hlt rates
  std::string m_ratesfolderName;  ///< Name of cool folder of hlt rates

  cool::IFolderPtr m_chainsfolder;  ///< Pointer to cool folder of hlt chain names
  std::string m_chainsfolderName;  ///< Name of cool folder of hlt chain names

  std::string m_ratestag;  ///< Cool tag for rates folder
  std::string m_chainstag;  ///< Cool tag for chains folder

  /// Cool record specification of HLT rates.
  cool::RecordSpecification m_recordSpec;

  /// Cool record specification of chain names
  cool::RecordSpecification m_chainrecordSpec;

  /// folder description string for run-lumi based folders
  const std::string m_folderDesc_runlumi = "<timeStamp>run-lumi</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>";

  /// folder description string for time based folders
  const std::string m_folderDesc_time = "<timeStamp>time</timeStamp><addrHeader><address_header service_type=\"71\" clid=\"40774348\" /></addrHeader><typeName>AthenaAttributeList</typeName>";

  /// Opens the database, create if it doesn't exist(only for sqlite)
  void openDatabase();

  /// Open the cool folder, create if it doesn't exist(only for sqlite)
  void openFolder();

 public:
  /// Constructor
  ///
  /// Once the constructor is called, it open the database, sets the record
  /// specification and opens the folder, or creats them if they don't exist.
  ///
  /// \param dbId Database connection string
  /// \param ratesfolderName Name of the cool folder for rates
  /// \param chainsfolderName Name of the cool folder for chain names
  /// \param ratestag Rates folder cool tag
  /// \param chainstag Chains folder cool tag
  HLTRatesCoolUtils(const std::string dbId,
                    const std::string ratesfolderName,
                    const std::string chainsfolderName,
                    const std::string ratestag, const std::string chainstag);
  ~HLTRatesCoolUtils();

  std::string getratesfoldername() {return m_ratesfolderName;}
  std::string getchainsfoldername() {return m_chainsfolderName;}
  std::string getratestag() {return m_ratestag;}
  std::string getchainstag() {return m_chainstag;}

  /// Create record spec
  void setRecordSpec();

  /// Write chain names in the database. Database is opened and closed at each write
  ///
  /// \param chains Vector of chain names as
  /// \param runnumber Current run number, the closing IOV will be set as
  ///                  the next run number, as we don't want a chainname
  ///                  data to span more than one run
  void writeChains(const std::vector<std::string> & vchains, const uint64_t runnumber);

  /// Write rates data in the database. Database is opened and closed at each write
  ///
  /// \param data HLT rates as a float vector.
  /// \param since timestamp of the publication time, which will be used as
  ///              the start of the IOV
  /// \param until timestamp for end of IOV, when it's 0, data is written
  ///              open ended (until the maximum IOV)
  void writeRates(const std::vector<float> & data, const uint64_t since, const uint64_t until = 0);
};
}  // namespace hlt2cool

#endif  // HLTRATES2COOL_HLTRATESCOOLUTILS_H_
