#!/usr/bin/env python2
"""
Test program that does:
- Read HLTRates2Cool configuration from OKS
- Publish ISS_TRP.HLT_Rate data every 10 seconds
- Sets ready4physics IS information to 0 and 1 (simulating ATLAS going into stable beams)
- Kills/restarts HLTRates2Cool application
- Stops run
- Runs tests over results

Tests are:
- Verify the averaging is done correctly,
- Verify that IOV boundaries are correct,
  * In case when stable beams start
  * In normal situation where each IOV is between 2 IS callbacks 60 seconds apart
  * In case where stable beam ends.
  * In case where run is started during stable beams
  * In case where run is stopped during stable beams

To run this program, start a partition with CustomLifetimeApplication@HLTRates2Cool_Application and ISS_TRP is_server.
- HLTRates2Cool parameters should be set to write results into an sqlite file.

"""
# python imports
import os
import re
import sys
import time
import random
import argparse
import threading

import numpy as np

# atlas tdaq imports
from ipc import IPCPartition, getPartitions
from ispy import *
import config
import libipcpy
#import libpbeastpy

script_dir = os.path.join(os.path.abspath(os.path.join(os.getcwd(), os.pardir)),'scripts')
sys.path.append(script_dir)

from PyCool import cool

from CoolHltRates import CoolHltRates

class HLTRateDataGenerator(object):
  """Generates data as it would be in HLTRateDataGenerator"""
  def __init__(self, number_of_chains = 100, mode = "iterative"):
    super(HLTRateDataGenerator, self).__init__()
    self.number_of_chains = number_of_chains
    self.mode = mode
    self.generator_count = 1 # Iterated by one at each generation
    self.fixed_value = 0 # Used as fixed value for rates if mode == "fixed"

    if self.mode not in ["iterative","random","fixed"]:
      raise HLTRateDataGenerator.NoSuchMode("Unknown mode for HLTRateDataGenerator: {0}".format(mode))
    if self.number_of_chains > 9999:
      raise HLTRateDataGenerator.TooManyChains("number_of_chains too high: {0}".format(number_of_chains))

    self.chain_names = []
    self.generate_chain_names()

    # Each chain has 7 rates. Providers and TriggerDB_Keys have 7 items each
    self.data = np.zeros(7 * (number_of_chains + 2), dtype=float)
    self.data[number_of_chains*7: (number_of_chains+1)*7] = [4200.,60.,60.,1, 0., 0., 0.] # Annotations in the end of data array
    self.data[(number_of_chains+1)*7 : (number_of_chains+2)*7] = [0.,0.,0.,0., 0., 0., 0.] # TriggerDB_Keys in the end

  def generate_chain_names(self):
    names = []
    for i in range(self.number_of_chains):
      self.chain_names.append("chains{:04d}".format(i))

    # These 2 are in the end of ISS_TRP.HLT_Rate info
    self.chain_names.append("Providers")
    self.chain_names.append("TriggerDB_Keys")

  def generate_data(self):
    for i in range(0,self.number_of_chains):
      if self.mode == "random":
         sub_array = 7 * [ random.random()* 10 ]
      elif self.mode == "iterative":
        # Use this mode to generate predictable data, for instance
        # if you want to test averaging mechanism of HLTRates2Cool
        # Example: 15th data point for 234th chain = 15.0234
        sub_array = 7 * [self.generator_count + 1E-4*i]
      elif self.mode == "fixed":
        sub_array = 7 * [self.fixed_value + 1E-4*i]

      self.data[i*7:(i+1)*7] = sub_array

    self.generator_count += 1

  class TooManyChains(Exception):
    pass

  class NoSuchMode(Exception):
    pass


class HLTRatePublisher(object):
  """docstring for HLTRatePublisher"""
  def __init__(self, datagen = None, partition = "part_hlt_cyildiz", interval = 10):
    super(HLTRatePublisher, self).__init__()
    self.partition, self.interval = partition, interval

    if not datagen:
      self.datagen = HLTRateDataGenerator()
    else:
      self.datagen = datagen

    p = IPCPartition(partition)
    self.is_hltrate = ISObject(p, 'ISS_TRP.HLT_Rate', 'TimePoint_IS')
    self.is_hltrate.XLabels   = self.datagen.chain_names
    self.is_hltrate.YLabels   = [ "input", "prescaled", "raw", "output", "rerun", "algoIn", "passedrerun" ]
    self.is_hltrate.RunNumber = 0 # HLTRates2Cool don't use this
    self.is_hltrate.LumiBlock = 0 # HLTRates2Cool don't use this
    self.is_hltrate.MetaData = [ "0", "0", "0", "0", "AthenaP1_21.0.0" ] # HLTRates2Cool don't use this

    self.last_publication_time = 0

  def publish(self):
    """ publish one data point """
    self.datagen.generate_data()
    self.is_hltrate.Data = data = self.datagen.data
    self.is_hltrate.TimeStamp = libipcpy.OWLTime()
    self.is_hltrate.checkin() #Publish to IS
    self.last_publication_time = self.is_hltrate.TimeStamp.c_time()

  def publish_N(self,number_of_publications = 10, start_at = 0):
    """ publish number_of_publications data points with defined interval"""

    # If start_at is not set, start immediately
    if start_at:
      sleep_until(start_at)

    for publication_count in range(number_of_publications):
      self.publish()
      sleep_until(self.last_publication_time+self.interval)


class Ready4PhysicsPublisher(object):
  """Publish true/false in RunParams.Ready4Physics"""
  def __init__(self, partition):
    super(Ready4PhysicsPublisher, self).__init__()

    p = IPCPartition(partition)

    self.publication_time = 0 # Timestamp when flag is changed

    self.is_r4p = ISObject(p, 'RunParams.Ready4Physics', 'Ready4PhysicsInfo')
    self.is_r4p.run_number = 0
    self.is_r4p.lumi_block = 0

    self.publish(False)

  def publish(self,flag):
    """ flag = True or False """
    self.is_r4p.ready4physics = flag

    self.publication_time =  int(time.time())
    self.is_r4p.checkin() #Publish to IS

class AppConfig(object):
  """Configuration for HLTRates2Cool"""
  def __init__(self,partition_name):
    super(AppConfig, self).__init__()
    self.partition_name = partition_name
    self.db_filename = "" # Full path of db filename
    self.dbname = "" # database name (CONDBR2 for production)
    self.chainstag = ""
    self.ratestag = ""
    self.db_connection_string = ""

    self.read_oks_configuration()
    if not self.db_filename or not self.chainstag or not self.ratestag:
      print("Can't get database file, exiting...")

  def __str__(self):
    doc_str = ("partition_name: {part_name}\ndb_filename: {dbf}, "
               "dbname: {dbn}, db_connection: {dbc}\n"
               "chain tag: {ct}, rate tag: {rt}" )
    return  doc_str.format(part_name = self.partition_name,
                           dbf = self.db_filename,
                           dbn = self.dbname,
                           dbc = self.db_connection_string,
                           ct = self.chainstag,
                           rt = self.ratestag)

  #Read configuration
  def read_oks_configuration(self):
    """ Read configuration of HLTRates2Cool_Application and fill members"""

    rdb_path = "rdbconfig:RDB@{0}".format(self.partition_name)
    p = IPCPartition(self.partition_name)

    try:
      db = config.Configuration(rdb_path)
    except:
      print("Can't get db at {0}".format(rdb_path))
      raise

    #Reading Partition default dir
    obj = self.partition_name
    oks_class = "Partition"
    try:
      cfg = db.get_obj(oks_class, obj)
      working_dir = cfg["WorkingDirectory"]
    except:
      print("Can't read object from db: {0}@{1}".format(oks_class,oks))
      raise

    #Reading HLTRates2Cool parameters
    obj = "HLTRates2Cool_Application"
    oks_class = "CustomLifetimeApplication"
    try:
      cfg = db.get_obj(oks_class, obj)
      params = cfg["Parameters"]
    except:
      print("Can't read object from db: {0}@{1}".format(oks_class,oks))
      raise

    # Parse arguments of HLTRates2Cool_Application in OKS
    try:
      custom_parser = argparse.ArgumentParser()
      custom_parser.add_argument('-d','--dbconnect',default="COOLONL_TRIGGER/CONDBR2")
      custom_parser.add_argument('-c','--chainstag',default="TriggerHltChains-Physics-01")
      custom_parser.add_argument('-r','--ratestag',default="TriggerHltRates-Physics-01")
      custom_parser.add_argument('-I','--interval',default="60")
      custom_args = custom_parser.parse_known_args(params.split())[0]
      self.chainstag = custom_args.chainstag
      self.ratestag = custom_args.ratestag
      self.db_connection_string = custom_args.dbconnect
    except:
      print("Can't parse arguments of HLTRates2Cool_Application")
      raise

    filename, self.dbname = self.get_filename_and_dbname_from_connection_string(self.db_connection_string)
    if filename.startswith("/"): # Then it must be an absolute path of filename
      self.db_filename = filename
    else: # Path relative to the working directory
      self.db_filename = os.path.join(working_dir,filename)

    self.db_connection_string =  "sqlite://;schema={};dbname={}".format(self.db_filename, self.dbname)

  @staticmethod
  def get_filename_and_dbname_from_connection_string(connection_string):
      try:
        match = re.search('sqlite://;schema=(.+);dbname=(.+)',connection_string)
        db_filename = match.group(1)
        db_name = match.group(2)
        return db_filename, db_name
      except:
        print("Can't get DB propertied from : {0}".format(connection_string))
        raise

def parse_arguments():
  parser = argparse.ArgumentParser()
  parser.add_argument('-p','--partition',help="Partition name",default="part_hlt_cyildiz")

  args = parser.parse_args()
  return args

def get_run_number(partition):
  ipc_partition = IPCPartition(partition)
  reader = InfoReader(ipc_partition, "RunParams", 'RunParams')
  reader.update()
  return reader.objects["RunParams.RunParams"]["run_number"]

def sleep_until(timestamp):
  seconds_to_sleep = timestamp - time.time()
  if seconds_to_sleep < 0:
    raise RuntimeError("sleep_until past timestamp is not possible")
  time.sleep(seconds_to_sleep)
  #print(time.time())

def next_timestamp_multiple_of_10():
  current_time = int(time.time())
  return current_time + 10 - current_time%10

def restart_app(partition):
  command = 'rc_sender -p {part} -n HLTRates2Cool_Segment -c RESTARTAPP "HLTRates2Cool_Application"'.format(part = partition)
  print(command)
  os.system(command)

def stop_partition(partition):
  command = 'rc_sender -p {part} -n RootController -c STOP'.format(part = partition)
  print(command)
  os.system(command)

def average_between(value0,value1):
  """ Gets average of all numbers between value0 and value1,
      example: average_between(5,8) = (5+6+7+8)/4.
  """
  if value0>=value1:
    raise RuntimeError("value0({}) should be smaller than value1({})".format(value0,value1))
  return (value0 + value1)/2.0

def main():

  args = parse_arguments()
  print(args)

  h2cool_config = AppConfig(args.partition)
  print(h2cool_config)

  run_number = get_run_number(args.partition)

  # It's enough to set SOR anytime before starting to publish ISS_TRP.HLTRate
  sor = int(time.time())
  print("{} - SOR".format(sor))

  # Dictionary of expected results, key: iov start/end, value: average base value of rates and extra information about IOV
  expected_results = {}

  r4p = Ready4PhysicsPublisher(args.partition)
  r4p.publish(False)

  hltrate_datagen = HLTRateDataGenerator(number_of_chains = 3500, mode = "iterative")
  hltrate_publisher = HLTRatePublisher(datagen = hltrate_datagen, partition = "part_hlt_cyildiz", interval = 10)

  # Start thread to do regular publications at next multiple of 10
  start_of_rate_publications = next_timestamp_multiple_of_10()
  threading.Thread(target=hltrate_publisher.publish_N,kwargs={'number_of_publications':100,'start_at':start_of_rate_publications}).start()

  # Test: Stop/Restart the app before stable beams and make sure it doesn't crash {{{

  time_to_restart_app = next_timestamp_multiple_of_10() + 5
  sleep_until(time_to_restart_app)
  restart_app(partition = args.partition)
  print("{} - Restart app before stable beams".format(time_to_restart_app))

  # }}}

  # Test: First IOV should start at timestamp of ready4physics {{{
  start_of_stable_beams = next_timestamp_multiple_of_10() + 3
  sleep_until(start_of_stable_beams)
  r4p.publish(True)
  print("{} - Stable beams: True".format(start_of_stable_beams))

  # Add first IOV's expected result
  start_of_iov = start_of_stable_beams - 1
  end_of_iov = next_timestamp_multiple_of_10() + 60
  start_value = hltrate_datagen.generator_count
  end_value = start_value + 6
  expected_results[(start_of_iov, end_of_iov)] = [average_between(start_value,end_value),"First IOV after stable beams"]
  sleep_until(end_of_iov + 3)

  # }}}

  # Test: Second IOV should starts at end of first IOV #{{{
  # Add second IOV's expected result
  start_of_iov = end_of_iov
  end_of_iov = start_of_iov + 60
  start_value = hltrate_datagen.generator_count
  end_value = start_value + 5
  expected_results[(start_of_iov, end_of_iov)] = [average_between(start_value,end_value),"Second IOV after stable beams"]

  sleep_until(end_of_iov + 3)

  # }}}

  # Test: IOV should end at the publication after stable beams end {{{
  start_of_iov = end_of_iov
  start_value = hltrate_datagen.generator_count

  end_of_stable_beams = end_of_iov + 33
  sleep_until(end_of_stable_beams)
  r4p.publish(False)
  print("{} - Stable beams: False".format(end_of_stable_beams))

  # Add third IOV's expected result
  end_value = hltrate_datagen.generator_count - 1
  end_of_iov = next_timestamp_multiple_of_10()
  expected_results[(start_of_iov, end_of_iov)] = [average_between(start_value,end_value),"IOV after end of stable beams"]

  sleep_until(end_of_iov + 3)

  # }}}

  # Sleep for 2 publications
  sleep_until(next_timestamp_multiple_of_10() + 10)

  # Test: App stops(restarts) 10 seconds after stable beams  # {{{
  # see if IOV is from stable beams to restart of the run
  start_of_stable_beams = next_timestamp_multiple_of_10() + 3
  sleep_until(start_of_stable_beams)
  r4p.publish(True)
  print("{} - Stable beams: True".format(start_of_stable_beams))

  current_value = hltrate_datagen.generator_count
  start_of_iov = start_of_stable_beams - 1

  time_to_restart_app = start_of_stable_beams + 10
  sleep_until(time_to_restart_app)
  restart_app(partition = args.partition)
  print("{} - Restart app during stable beams".format(time_to_restart_app))

  end_of_iov = time_to_restart_app # App should write current data to COOL if it's stopped properly
  expected_results[(start_of_iov, end_of_iov)] = [current_value,"IOV, where app restarted after single publication during stable beams"]

  # }}}

  # Test: App starts during stable beams, IOV should start at the app-start time {{{
  start_of_iov = time_to_restart_app
  end_of_iov = next_timestamp_multiple_of_10() + 60

  start_value = hltrate_datagen.generator_count
  end_value = start_value + 6

  expected_results[(start_of_iov, end_of_iov)] = [average_between(start_value,end_value),"Next IOV after app restarted during stable beams"]

  sleep_until(end_of_iov + 3)

  # }}}

  # Test: Stable beams end right after a COOL publication, check there is no extra publication after # {{{

  start_of_iov = end_of_iov
  end_of_iov = start_of_iov + 60
  end_of_stable_beams = end_of_iov + 5

  start_value = hltrate_datagen.generator_count
  end_value = start_value + 5

  expected_results[(start_of_iov, end_of_iov)] = [average_between(start_value,end_value),"End of stable beams right after COOL publication: There should not be another IOV right after this one!"]

  sleep_until(end_of_stable_beams)
  r4p.publish(False)
  print("{} - Stable beams: False".format(end_of_stable_beams))

  #}}}

  # Start stable beams again, do one publication {{{
  start_of_stable_beams = end_of_iov + 45
  start_of_iov = start_of_stable_beams - 1

  start_value = hltrate_datagen.generator_count
  end_value = hltrate_datagen.generator_count + 6

  sleep_until(start_of_stable_beams)
  r4p.publish(True)
  print("{} - Stable beams: True".format(start_of_stable_beams))

  end_of_iov = next_timestamp_multiple_of_10() + 60
  sleep_until(end_of_iov + 3)

  expected_results[(start_of_iov, end_of_iov)] = [average_between(start_value,end_value),"A regular IOV"]

  # }}}

  # Test: Run stops during stable beams, make sure there is a publication at stop of the run #{{{
  time_to_stop_partition = end_of_iov + 35

  start_of_iov = end_of_iov
  end_of_iov = time_to_stop_partition
  start_value = hltrate_datagen.generator_count

  sleep_until(time_to_stop_partition)

  stop_partition(args.partition)
  print("{} - Run Stop".format(time_to_stop_partition))

  end_value = hltrate_datagen.generator_count - 1

  expected_results[(start_of_iov, end_of_iov)] = [average_between(start_value,end_value),"IOV at run stop"]

  eor = time_to_stop_partition + 30 # Not sure the run will stop immediately, give 30 second extra time
  print("{} - EOR".format(eor))

  # }}}

  keys = expected_results.keys()
  keys.sort()
  for key in keys:
    print("IOV: {}, result and explanation: {}".format(key,expected_results[key]))

  print("Information from the COOL file: " )
  CoolHltRates(runno=run_number, sor = sor, eor = eor,
                ratetag = h2cool_config.ratestag,
                chaintag = h2cool_config.chainstag,
                dbconnect = h2cool_config.db_connection_string,
                verbose = True)

if __name__ == "__main__":
    main()

