#!/usr/bin/env python2
# Program to test the classes and methods in the livetest

from hltrates2cool_livetest import HLTRateDataGenerator, HLTRatePublisher

import unittest

class hlttest_test(unittest.TestCase):

    def setUp(self): #Set up object or whatever needed(not needed if you test single func)
        pass

    #Each test function MUST start with test, rest is not important
    def testChainNames(self):
      hltrates_data = HLTRateDataGenerator(number_of_chains = 100, mode = "random")
      chainname_list = hltrates_data.chain_names
      self.assertEqual(len(chainname_list),102)
      self.assertEqual(chainname_list[14],"chains0014")

    def testExceptions2(self):
      with self.assertRaises(HLTRateDataGenerator.NoSuchMode) as cm:
        h = HLTRateDataGenerator(mode = "radnom")

    def testExceptions3(self):
      with self.assertRaises(HLTRateDataGenerator.TooManyChains) as cm:
        h = HLTRateDataGenerator(number_of_chains = 19999)

    def testRandomData(self):
      hltrates_data = HLTRateDataGenerator(number_of_chains = 100, mode = "random")

      hltrates_data.generate_data()
      data = hltrates_data.data
      self.assertEqual(len(data), 714)
      self.assertNotEqual(data[10],data[20])

    def testIterativeData(self):
      hltrates_data = HLTRateDataGenerator(number_of_chains = 60, mode = "iterative")

      hltrates_data.generate_data()
      data = hltrates_data.data
      self.assertEqual(data[0], 1.0000)
      self.assertEqual(data[1*7], 1.0001)
      self.assertEqual(data[10*7], 1.0010)
      self.assertEqual(data[59*7], 1.0059)
      self.assertEqual(data[60*7], 4200.)

      hltrates_data.generate_data()
      data = hltrates_data.data
      self.assertEqual(data[0], 2.0000)
      self.assertEqual(data[1*7], 2.0001)
      self.assertEqual(data[10*7], 2.0010)
      self.assertEqual(data[59*7], 2.0059)
      self.assertEqual(data[60*7], 4200.)

      hltrates_data.generate_data()
      hltrates_data.generate_data()
      hltrates_data.generate_data()

      data = hltrates_data.data
      self.assertEqual(data[0], 5.0000)
      self.assertEqual(data[1*7], 5.0001)
      self.assertEqual(data[10*7], 5.0010)
      self.assertEqual(data[59*7], 5.0059)
      self.assertEqual(data[60*7], 4200.)

    def testFixedData(self):
      hltrates_data = HLTRateDataGenerator(number_of_chains = 60, mode = "fixed")
      hltrates_data.fixed_value = 123

      hltrates_data.generate_data()
      data = hltrates_data.data
      self.assertEqual(data[1*7], 123.0001)
      self.assertEqual(data[12*7], 123.0012)
      self.assertEqual(data[47*7], 123.0047)
      self.assertEqual(data[59*7], 123.0060)
      self.assertEqual(data[60*7], 4200.)

      # Generate data 2 more times
      hltrates_data.generate_data()
      hltrates_data.generate_data()
      data = hltrates_data.data

      self.assertEqual(data[1*7], 123.0001)
      self.assertEqual(data[12*7], 123.0012)
      self.assertEqual(data[47*7], 123.0047)
      self.assertEqual(data[59*7], 123.0060)
      self.assertEqual(data[60*7], 4200.)


#To run the tests:
unittest.main()

