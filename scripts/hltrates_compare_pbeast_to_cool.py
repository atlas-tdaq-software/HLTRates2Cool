#!/usr/bin/python
#Script to compare the HLT Rates in IS repository(from pbeast) and COOL.

# One need to setup LCG environment(for PyCool) and tdaq environment (for pbeast). Following is needed for GPN:
# export PBEAST_SERVER_SSO_SETUP_TYPE=AutoUpdateKerberos
# ./hltrates_compare_pbeast_to_cool.py -r 327490

import sys
import argparse
import time
from time import sleep
from time import localtime
import signal
import re
import os
import matplotlib as mpl
import matplotlib.pyplot as pl
import matplotlib.ticker as ticker

def format_date(x, pos=None):
    return time.strftime( "%H:%M:%S", time.localtime( x ) )

parser = argparse.ArgumentParser()
parser.add_argument('-r','--runno',help="Run Number",required=True)
parser.add_argument('-c','--chainname',help="Chain Name to compare.",default="total")
parser.add_argument('-s','--server', default='https://atlasop.cern.ch',
                    help="Pbeast server url. For GPN: https://atlasop.cern.ch, for P1: http://pc-tdq-bst-05.cern.ch:8080")

args = parser.parse_args()

#mychain="HLT_j0_perf_boffperf_L1RD0_EMPTY"
mychain="grp_Calibration"
mychain="str_express_express"
mychain="grp_Cosmic_Muon"
mychain="total"

mychain = args.chainname


fig = pl.figure()

#{{{ Cool 
from CoolHltRates import CoolHltRates
c = CoolHltRates(args.runno)

sor = int(c.sor)
eor = int(c.eor)
if eor==0: #If run is ongoing, set eor=NOW
  eor = int(time.time())

print("SOR: {0}, EOR: {1}".format(sor,eor))

chains = c.chains
in_mychain = chains.index(mychain)
rates = []
timestamps = []
timestampsend = []
rate=0
until=0
for i in range(len(c.ratesarray)):
  #Don't use entries out of run boundries
  if c.sincearray[i] < sor or c.untilarray[i] > eor:
    continue
  rate = c.ratesarray[i][1+in_mychain]
  rates.append(rate)

  #ts = (c.sincearray[i] + c.untilarray[i])/2
  since = c.sincearray[i]
  until = c.untilarray[i]
  timestamps.append(since)
  timestampsend.append(until)

#Option 1: Plot the step function, add last entry once more with IOV end (Otherwise last step will not be drawn)
#rates.append(rate)
#timestamps.append(until)
#pl.step(timestamps,rates,color="blue",label="cool",where="post",lw=5,alpha=0.5)

#Option 2: Plot each IOV seperately, better to show if there are missing IOVs in between
for i in range(len(rates)):
  pcool = pl.plot([timestamps[i],timestampsend[i]],[rates[i],rates[i]],color="blue",label="cool",lw=4,alpha=0.5)
#}}}

#{{{ pbeast
import libpbeastpy
print("Getting data from pbeast")
pbeast = libpbeastpy.ServerProxy(args.server)

mydata = dict()
attributes = ['RunNumber','TimeStamp','LumiBlock','XLabels','YLabels','Data','MetaData']
#attributes = ['TimeStamp','XLabels','Data']

for attr in attributes:
  # Data from start of run to end of run
  mydata[attr] = pbeast.get_data('ATLAS', 'TimePoint_IS', attr, 'ISS_TRP.HLT_Rate', False, sor * 1000000, eor * 1000000)

index = 0
RunNumber =  mydata['RunNumber'][0][4]['ISS_TRP.HLT_Rate'][index][1]
XLabels =  mydata['XLabels'][0][4]['ISS_TRP.HLT_Rate'][index][1]
#LumiBlock =  mydata['LumiBlock'][0][4]['ISS_TRP.HLT_Rate'][index][1]
#MetaData =  mydata['MetaData'][0][4]['ISS_TRP.HLT_Rate'][index][1]

in_mychain = XLabels.index(mychain)

rates = []
timestamps = []
for i in range(len(mydata['Data'][0][4]['ISS_TRP.HLT_Rate'])):

  #Get Timestamp from the pbeast information 
  ts =  mydata['Data'][0][4]['ISS_TRP.HLT_Rate'][i][0]/1E6 #0th element is timestamp
  if ts < sor or ts > eor: #Don't use if timestamp is not inside the current run
    continue

  timestamps.append(ts)

  Data =  mydata['Data'][0][4]['ISS_TRP.HLT_Rate'][i][1]
  rate = Data[in_mychain*7+3]
  rates.append(rate)

ppbeast = pl.plot(timestamps,rates,color="red",label="pbeast",alpha=0.5)
#}}}

ax = pl.gca()
ax.xaxis.set_major_formatter(ticker.FuncFormatter(format_date))
fig.autofmt_xdate() # To rotate nicely
pls = pcool + ppbeast
labs = [p.get_label() for p in pls]
pl.legend(pls,labs,loc=0)
pl.title("HLT Rates for Run: {0}, Chain: {1}".format(args.runno,mychain))
pl.savefig('test{0}_{1}.png'.format(args.runno,mychain), bbox_inches='tight')
pl.show()
