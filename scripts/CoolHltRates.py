#!/usr/bin/python
# This program is used to read HLT rates in COOL database, written by atlas-tdaq-software/HLTRates2Cool project
# One needs to set up LCG or ATLAS Offline software to use PyCool

import sys
import time
import array
import datetime
import argparse
from PyCool import cool,coral

class CoolHltRates(object):
  """HLT Rates information in the cool"""
  def __init__(self, runno, sor= -1, eor= -1 ,
                ratefolderName = "/TRIGGER/HLT/Rates", 
                chainfolderName = "/TRIGGER/HLT/Chains",
                ratetag = "TriggerHltRates-Physics-01",
                chaintag = "TriggerHltChains-Physics-01", 
                dbconnect = "COOLONL_TRIGGER/CONDBR2",
                verbose=False):
    super(CoolHltRates, self).__init__()
    self.runno = runno
    self.sor= sor  #Start of Run
    self.eor= eor  #End of Run
    self.ratefolderName = ratefolderName
    self.chainfolderName = chainfolderName
    self.ratetag = ratetag
    self.chaintag = chaintag
    self.dbconnect = dbconnect
    self.verbose = verbose

    self.chains=[]     # List of chain names
    self.ratesarray=[] # List of lists, each corresponding to rates of 1 IOV
    self.sincearray=[] # List of beginning of IOVs
    self.untilarray=[] # List of end of IOVs
    self.create_arrays(int(runno))

  #global constants
  def create_arrays(self,runno):
    """ Fills following members
        - chains: A list of chain names as strings ordered as in 'XLabels' 
          of ISS_TRP.HLT_Rate.
        - ratesarray: A list of lists, each belonging to rates of one IOV in COOL.
        - sincearray: A list of ints, containing "since" timestamp of IOV
        - untilarray: A list of ints, containing "until" timestamp of IOV
    """

    coolchannel = 0
    # Use the following for the test runs
    # self.ratetag = "TestTagRate2018"
    # self.chaintag = "TestTagChain2018"

    dbSvc=cool.DatabaseSvcFactory.databaseService()

    readonly = True

    try:
      db=dbSvc.openDatabase(self.dbconnect,readonly)
      chainfolder=db.getFolder(self.chainfolderName)
      ratefolder=db.getFolder(self.ratefolderName)
    except Exception,e:
      print("Can't open DB or get folders")
      print("Exception: {0}".format(e))
      sys.exit(0)

    # Chain folder is run/lumi based, getting entry during the run
    lb=2
    runlb = (runno << 32) + lb

    try:
      chainobj = chainfolder.findObject(runlb, coolchannel, self.chaintag);
    except Exception,e:
      print("Can't get chains object from chain folder!")
      print("Exception: {0}".format(e))
      sys.exit(0)

    since = chainobj.since()
    runnumber = since >> 32
    until = chainobj.until()
    channel = chainobj.channelId() # In chain folder, channel should be always 0
    payload = chainobj.payload()
    chains = payload["chains"].split(",")
    print(" runnumber: {0} / Chain Names(first 3, last 3): {1} ... {2} ".format(runnumber,chains[0:3],chains[-3:]))

    # Getting run start end times for the rate folder
    sor_time,eor_time = self.get_sor_eor(runno)
    print("sor: {0}, eor: {1}".format(sor_time,eor_time))
    iov_start = int(sor_time)
    if eor_time == 0:
      iov_end = int(time.time()) # Set it to current timestamp
    else:
      iov_end = int(eor_time)

    # 341649 is the last physics run of 2017
    # Starting 2018, the iov of rate folder is in ns instead of s.
    if runno > 341649:
        iov_start = long(sor_time*1E9)
        iov_end = long(iov_end*1E9)

    try:
      rateobjs = ratefolder.browseObjects( iov_start, iov_end,cool.ChannelSelection(int(coolchannel)),self.ratetag)
    except Exception,e:
      print("Can't get rates objects from folder!")
      print("Exception: {0}".format(e))
      sys.exit(0)

    ratearray = []
    sincearray = []
    untilarray = []
    for obj in rateobjs:
      since = obj.since() # USER: this can be used as timestamp of the entry
      until = obj.until()
      # Discard last entry of the previous run, or first entry of the next run, which may fall in (iov_start,iov_end)
      if since < iov_start or until > iov_end:
        if self.verbose:
          print("Discarding IOV: {0}-{1}".format(since,until))
        continue
      channel = obj.channelId() #Expected to be 0

      payload = obj.payload()
      data = payload["rates"]

      # Convert blob into array of floats
      datastr = data.read()
      floatarr = array.array("f", datastr).tolist()
      ratearray.append(floatarr)
      sincearray.append(since)
      untilarray.append(until)

      # Always return seconds to user
      #Following can be used to print all information about the entry
      if self.verbose:
        print("since-until: {1}-{2} / data(first 5, last 5 data): {3},{4}".format(channel,since,until, floatarr[0:5],floatarr[-5:]))

    if runno > 341649:
      sincearray = [s/1E9 for s in sincearray]
      untilarray = [u/1E9 for u in untilarray]

    if ratearray==[]:
      raise Exception("Rate folder is empty for run: {0}".format(runno))

    db.closeDatabase()
    self.chains = chains
    self.ratesarray = ratearray
    self.sincearray = sincearray
    self.untilarray = untilarray

  def get_sor_eor(self,runno):
    """Returns a tuple with run start time,end time for 'runno'
      Normally, one expects the run to be still ongoing.
      If the run is still ongoing, end time will be set to 0"""

    # If they are already set, just return
    if self.sor != -1 and self.eor != -1:
      return self.sor, self.eor

    sorfolder ="/TDAQ/RunCtrl/SOR"
    eorfolder ="/TDAQ/RunCtrl/EOR"
    dbconnect = "COOLONL_TDAQ/CONDBR2"
    coolchannel = 0 #Folders are single channel

    dbSvc=cool.DatabaseSvcFactory.databaseService()

    readonly = True

    try:
      db=dbSvc.openDatabase(dbconnect,readonly)
    except Exception,e:
      print("Can't open DB: {0}".format(dbconnect))
      print("Exception: {0}".format(e))
      sys.exit(0)

    try:
      cfolder_sor=db.getFolder(sorfolder)
    except Exception,e:
      print("Can't get folder: {0}".format(sorfolder))
      print("Exception: {0}".format(e))
      sys.exit(0)

    try:
      cfolder_eor=db.getFolder(eorfolder)
    except Exception,e:
      print("Can't get folder: {0}".format(eorfolder))
      print("Exception: {0}".format(e))
      sys.exit(0)

    # Find object at lb=2 of the run
    lb=2
    runlb = (runno << 32) + lb

    try:
      obj = cfolder_sor.findObject(runlb, coolchannel)
    except Exception,e:
      print("Can't get object from folder: {0}".format(sorfolder))
      print("Exception: {0}".format(e))
      sys.exit(0)

    print("Data:")
    payload = obj.payload()

    channel = obj.channelId()

    since = obj.since()
    runnumber = since >> 32
    lb1 = since - (runnumber<<32)
    until = obj.until()
    runnumber2 = until >> 32
    lb2 = until - (runnumber2<<32)

    payload = obj.payload()

    # Following may be needed if for any reason the SOR COOL folder isn't written yet
    if payload["RunNumber"]!=runno:
      raise ValueError("Run number doesn't match!. Expected: {0}, Received: {1}".format(runno, payload["RunNumber"]))

    sor_time = payload["SORTime"]/1E9
    print("SOR: {0} ".format(sor_time))
        
    eor_time = 0
    try:
      obj = cfolder_eor.findObject(runlb,coolchannel)
      payload = obj.payload()
      eor_time = payload["EORTime"] / 1E9
      print("End of Run reached, run is stopped! EOR: {0}".format(eor_time))
    except Exception,e:
      print("End of Run NOT reached, run ongoing as expected")

    # finish
    db.closeDatabase()
    self.sor = sor_time
    self.eor = eor_time
    return sor_time,eor_time


def main():
  parser = argparse.ArgumentParser()
  parser.add_argument('-r','--runno',type=int, help="Run Number",required=True)

  args = parser.parse_args() 
  runno = args.runno
  print(args)
  
  c = CoolHltRates(args.runno)

  chains = c.chains
  rates = c.ratesarray
  timestamps = c.sincearray

  #Example: Printing rate of a specific chain(str_L1Calo_physics)

  #check data format version
  version = rates[0][0]
  if version == 1.0: # Version 1.0 has all 7 rate items archived
    rateitems = ["input", "prescaled", "raw", "output", "rerun", "algoIn", "passedrerun"]
  elif version == 2.0: # Starting from version 2.0, only output rates are archived
    rateitems = ["output"]
  else:
    raise ValueError('Version number {0} not known'.format(version))

  # Version 2.0 format:
  # rateobj[0] -> version number (2.0)
  # rateobj[1:nchains+1] -> Output rate of chains (nchains = number of chains)
  # rateobj[nchains+1:] -> Providers (Number of summed histograms at each level, normally it is 4 entries)

  # One has to first find index
  mychain = "total"
  try:
    print("Rates for Chain: {}".format(mychain))

    # Formatted header
    print("{:>12s}".format("Timestamp")),
    for item in rateitems:
      print("{:>15s}".format(item)),
    print("")
    in_mychain = chains.index(mychain)
    j = 0
    for rateobj in rates: #Loop over each IOV
      print("{:12d}".format(timestamps[j])),
      if version == 1.0:
        for item in rateobj[in_mychain*7+1:(in_mychain+1)*7+1]:
          print("{:15.3f}".format(item)),
      elif version == 2.0:
        item = rateobj[in_mychain+1]
        print("{:15.3f}".format(item))
      j+=1
  except ValueError:
    print("Chain {} not found...".format(mychain))

if __name__ == "__main__":
    main()

