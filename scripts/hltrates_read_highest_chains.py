#!/usr/bin/env python2
# Script to plot HLT Rates of multiple chains for a single run

import sys
import argparse
import time
from time import sleep
from time import localtime
import signal
import re
import os
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as pl
import matplotlib.ticker as ticker

def format_date(x, pos=None):
    return time.strftime( "%H:%M:%S", time.localtime( x ) )

parser = argparse.ArgumentParser()
parser.add_argument('-r','--runno',help="Run Number",required=True)
parser.add_argument('-n','--numberofchains',help="Number of chains to show",type=int,default=10)
parser.add_argument('-s','--startswith',help="Chain Name starts with",default="HLT")

args = parser.parse_args()

fig = pl.figure(figsize=(13,6))
ax = pl.axes([0.03,0.1,0.7,0.80])

#{{{ Cool 
from CoolHltRates import CoolHltRates
c = CoolHltRates(args.runno)

sor = int(c.sor)
eor = int(c.eor)
if eor==0: #If run is ongoing, set eor=NOW
  eor = int(time.time())

print("SOR: {0}, EOR: {1}".format(sor,eor))

nchains = len(c.chains)

#Plotting chains with highest rate
nprates = np.array(c.ratesarray).transpose()[1:nchains+1] # order as [chain][timestamp] & drop the version number from beginning, and provider from the end

# Only check chain names that start with
mask = np.array([i.startswith(args.startswith) for i in c.chains]) # Boolean mask of chain names that start with ...
nprates = nprates[mask] # Pick only chain rates for those chains
chainnames = np.array(c.chains)[mask] # Pick only those chain names

avg = nprates.mean(1) # Take mean of each chain and put in an array
maxindex=avg.argsort() # Make array of indices from highest to lowest values in avg
timestamps = c.sincearray

for ind in maxindex[:-args.numberofchains-1:-1]:
  pl.plot(timestamps,nprates[ind],lw=3,alpha=0.5,label=chainnames[ind][0:28])
  print(chainnames[ind])

#}}}

ax = pl.gca()
ax.xaxis.set_major_formatter(ticker.FuncFormatter(format_date))
fig.autofmt_xdate() # To rotate nicely
pl.legend(loc=0, bbox_to_anchor = (1.0, 1.0))
pl.title("Highest HLT Rates for Run: {0}".format(args.runno))
pl.savefig('highestchains_{0}_startswith_{1}.png'.format(args.runno,args.startswith), bbox_inches='tight')
pl.show()
