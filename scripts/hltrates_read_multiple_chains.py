#!/usr/bin/env python2
# Script to plot HLT Rates of multiple chains for a single run

import sys
import argparse
import time
from time import sleep
from time import localtime
import signal
import re
import os
import matplotlib as mpl
import matplotlib.pyplot as pl
import matplotlib.ticker as ticker

def format_date(x, pos=None):
    return time.strftime( "%H:%M:%S", time.localtime( x ) )

parser = argparse.ArgumentParser()
parser.add_argument('-r','--runno',help="Run Number",required=True)
parser.add_argument('-c','--chainname',help="Chain Name regular expression pattern to search.",default="grp_Cos.*")

args = parser.parse_args()

pattern = args.chainname

fig = pl.figure(figsize=(13,6))
ax = pl.axes([0.03,0.1,0.7,0.80])

#{{{ Cool 
from CoolHltRates import CoolHltRates
c = CoolHltRates(args.runno)

sor = int(c.sor)
eor = int(c.eor)
if eor==0: #If run is ongoing, set eor=NOW
  eor = int(time.time())

print("SOR: {0}, EOR: {1}".format(sor,eor))

chains = c.chains
indices ={}
rates = {}

for chain in chains:
  if re.match(pattern,chain):
    indices[chain] = chains.index(chain)
    rates[chain]=[]
    print("Chain found: {0}".format(chain))

timestamps = []
rate=0
until=0
for i in range(len(c.ratesarray)):
  #Don't use entries out of run boundries
  if c.sincearray[i] < sor or c.untilarray[i] > eor:
    continue

  for chain in indices.keys():
    rates[chain].append(c.ratesarray[i][1+indices[chain]])

  #ts = (c.sincearray[i] + c.untilarray[i])/2
  ts = c.sincearray[i]
  until = c.untilarray[i]
  timestamps.append(ts)

for chain in indices.keys():
  pl.plot(timestamps,rates[chain],lw=3,alpha=0.5,label=chain[0:28])
#}}}

ax = pl.gca()
ax.xaxis.set_major_formatter(ticker.FuncFormatter(format_date))
fig.autofmt_xdate() # To rotate nicely
pl.legend(loc=0, bbox_to_anchor = (1.0, 1.0))
pl.title("HLT Rates for Run: {0}, Pattern: {1}".format(args.runno,pattern))
#pl.savefig('test{0}_{1}.png'.format(args.runno,mychain), bbox_inches='tight')
pl.show()
