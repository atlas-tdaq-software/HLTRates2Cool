#!/usr/bin/env tdaq_python
# This app reads from pbeast and publishes to ISS_TRP IS Server. It's done to test using ISS_TRP.HLT_Rate in absence of HLTAdapter in the local partition
#
# For GPN/Testbed, before running this, do:
# export PBEAST_SERVER_SSO_SETUP_TYPE=AutoUpdateKerberos
# ./hltrates_pbeast_to_isstrp.py -p part_hlt_cyildiz -t 5 -s "https://atlasop.cern.ch"

import sys
from ispy import *
import argparse
from time import sleep
from time import localtime
import signal
import re
import os
import libipcpy
import libpbeastpy

parser = argparse.ArgumentParser()
parser.add_argument('-p','--partition',help="Partition name",required=True)
parser.add_argument('-t','--time', type=int, help="Update interval", default=10)
parser.add_argument('-s','--server', default='http://pc-tdq-bst-05.cern.ch:8080',
                    help="Pbeast server url. For GPN: https://atlasop.cern.ch, for P1: http://pc-tdq-bst-05.cern.ch:8080")

args = parser.parse_args()

test = libpbeastpy.ServerProxy(args.server)

mydata = dict()
attributes = ['RunNumber','TimeStamp','LumiBlock','XLabels','YLabels','Data','MetaData']

for attr in attributes:
  # Data from 5th to 94th LB in a run
  mydata[attr] = test.get_data('ATLAS', 'TimePoint_IS', attr, 'ISS_TRP.HLT_Rate', False, 1472806626956843, 1472811921991775)

index = 0
#Following are fixed as they don't change during the run (Fixed LB for convenience)
XLabels =  mydata['XLabels'][0][4]['ISS_TRP.HLT_Rate'][index][1]
YLabels =  mydata['YLabels'][0][4]['ISS_TRP.HLT_Rate'][index][1]
RunNumber =  mydata['RunNumber'][0][4]['ISS_TRP.HLT_Rate'][index][1]
LumiBlock =  mydata['LumiBlock'][0][4]['ISS_TRP.HLT_Rate'][index][1]
MetaData =  mydata['MetaData'][0][4]['ISS_TRP.HLT_Rate'][index][1]

myp = IPCPartition(args.partition)

my_tp = ISObject(myp, 'ISS_TRP.HLT_Rate', 'TimePoint_IS')

my_tp.XLabels   = XLabels
my_tp.YLabels   = YLabels
my_tp.RunNumber = RunNumber
my_tp.LumiBlock = LumiBlock
my_tp.MetaData = MetaData

for i in range(len(mydata['Data'][0][4]['ISS_TRP.HLT_Rate'])):

  print("Publishing new data")
  #Data and timestamp are updated at each iteration
  Data =  mydata['Data'][0][4]['ISS_TRP.HLT_Rate'][i][1]
  TimeStamp = libipcpy.OWLTime()

  my_tp.TimeStamp = TimeStamp
  my_tp.Data = Data
  my_tp.checkin() #Publish to IS
  sleep(args.time)

