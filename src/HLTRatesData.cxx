#include <time.h>
#include <algorithm>
#include <iterator>
#include "HLTRates2Cool/HLTRatesData.h"

ERS_DECLARE_ISSUE(hlt2cool,
                  IS_Issue,
                  "HLTRates2Cool IS Receiver Issue: " << message,
                  ((const char *)message)
                  )

hlt2cool::HLTRatesData::HLTRatesData(ISCallbackInfo* isc) {
  update(isc);
}

void hlt2cool::HLTRatesData::createChainMap() {
  int j = 0;
  for (auto c : m_chains) {
    m_chain_map[c] = j;
    j++;
  }
}

void hlt2cool::HLTRatesData::update(ISCallbackInfo * isc) {
  ERS_DEBUG(0, "Updating class data");

  std::string provider(isc->name());

  ERS_DEBUG(0, "First update? : " << m_first);
  if (isc->type().name() == "HistogramData<float>") {
    oh::HistogramData<float> h;
    isc->value(h);
    update(h);
  } else if (isc->type().name() == "TimePoint_IS") {
    TimePoint_IS tp;
    isc->value(tp);
    update(tp);
  } else {
    std::string s = "Unknown IS type: " + isc->type().name() + ". Expected HistogramData<float> or TimePoint_IS.";
    hlt2cool::IS_Issue issue(ERS_HERE, s.c_str());
    ers::fatal(issue);
    exit(0);
  }
}

void hlt2cool::HLTRatesData::update(const oh::HistogramData<float>& h) {
  // Publication time of the histogram
  m_pub_time = h.time().c_time();

  // Should be done only at the first iteration
  // then next iterations should check the equivalency
  unsigned Nxbins, Nybins;
  Nxbins = h.get_bin_count(oh::Axis::X);
  Nybins = h.get_bin_count(oh::Axis::Y);
  if (m_first) {
    m_chains = h.get_labels(oh::Axis::X);
    m_rateitems  = h.get_labels(oh::Axis::Y);

    if (m_rateitems.size() != m_nrateitems) {
      std::string s = "Number of rate items:" + std::to_string(m_rateitems.size())
                      + " Expected value: " + std::to_string(m_nrateitems);
      hlt2cool::IS_Issue issue(ERS_HERE, s.c_str());
      ers::fatal(issue);
      exit(0);
    }

    // To make the format of rate20 similar to ISS_TRP, setting last element as Providers
    // It's a bit ugly, but no better way...
    m_chains[m_chains.size()-1] = "Providers";

    createChainMap();

    m_first = 0;
  } else {
    std::vector<std::string> current_chains = h.get_labels(oh::Axis::X);
    current_chains[current_chains.size()-1] = "Providers";

    if (m_chains != current_chains) {
      hlt2cool::IS_Issue issue(ERS_HERE, "HLT Chains are not the same as the original list!");
      ers::fatal(issue);
      exit(0);
    }
    if (m_rateitems != h.get_labels(oh::Axis::Y)) {
      hlt2cool::IS_Issue issue(ERS_HERE, "HLT rate items are not the same as the original list!");
      ers::fatal(issue);
      exit(0);
    }
  }

  std::vector<std::pair<std::string, std::string>>  annotations;
  h.get_annotations(annotations);

  // Add the second element of annotations (number of providers)
  m_providers.clear();
  for (auto a : annotations) {
    m_providers.push_back(std::stoi(a.second));
  }

  // Pointer to array of floats, it has the undeflow and overflow bins too
  // First Nxbins+2 values, are underflow, so information of chain[i], rate[j] is:
  // pdata[(Nxbins+2)*(j+1) + i+1 ]
  float * pdata =  h.get_bins_array();

  m_rates.resize(Nxbins * Nybins);

  for (unsigned chain = 0; chain < Nxbins; chain++) {  // Loop over chains
    for (unsigned i = 0; i < Nybins; i++) {  // Loop over rates
        m_rates[chain*m_nrateitems+i] = pdata[(Nxbins+2)*(i+1) + chain+1 ];
    }
  }

  // Putting annotations in the vector instead of time element
  // This is to make format of the vector same to one provided in ISS_TRP
  unsigned ii = 0;
  for (auto p : m_providers) {
    m_rates[(Nxbins-1)*m_nrateitems+(ii++)] = p;
  }
  // Fill the rest with 0
  for (unsigned i = ii; i < m_nrateitems; i++) {
    m_rates[(Nxbins-1)*m_nrateitems+i] = 0.;
  }

  // TODO(cyildiz): Add the trigger keys as one more element

  // unsigned entries = h.get_entries();
}

void hlt2cool::HLTRatesData::update(const TimePoint_IS & tp) {
  // Publication time of the histogram
  OWLTime t_owl = tp.TimeStamp;

  m_pub_time = t_owl.total_mksec_utc()/1E6;  // Convert usec to sec since epoch

  if (m_first) {
    m_chains = tp.XLabels;
    m_rateitems  = tp.YLabels;

    if (m_rateitems.size() != m_nrateitems) {
      std::string s = "Number of rate items:" + std::to_string(m_rateitems.size())
                      + " Expected value: " + std::to_string(m_nrateitems);
      hlt2cool::IS_Issue issue(ERS_HERE, s.c_str());
      ers::fatal(issue);
      exit(0);
    }
    createChainMap();

    // Total bins are +2, as bin 0 is underflow, bin N+1 is overflow
    m_first = 0;
  } else {
    if (m_chains != tp.XLabels) {
      hlt2cool::IS_Issue issue(ERS_HERE,
                  "HLT Chains are not the same as the original list!");
      ers::fatal(issue);
      exit(0);
    }
    if (m_rateitems != tp.YLabels) {
      hlt2cool::IS_Issue issue(ERS_HERE,
                "HLT rate items are not the same as the original list!");
      ers::fatal(issue);
      exit(0);
    }
  }

  // Data element of TimePoint_IS is a vector of floats
  m_rates = tp.Data;

  // Last 2 elements of ISS_TRP:  Providers, TriggerDB_Keys

  // Fill the provider vector from the relevant part in the data vector
  m_providers.clear();
  unsigned index = m_chain_map["Providers"];
  std::copy_if(m_rates.begin() + index * m_nrateitems, m_rates.begin() + (index+1) * m_nrateitems,
              back_inserter(m_providers), [](float x) {return (x != 0);});
}

void hlt2cool::HLTRatesData::reset() {
  if (m_first) {
    std::string s = "HLTRatesData not updated yet, reset is invalid!";
    hlt2cool::IS_Issue issue(ERS_HERE, s.c_str());
    ers::error(issue);
    return;
  }

  for (auto iter = m_rates.begin(); iter != m_rates.end(); ++iter) {
    *iter = 0;
  }

  for (auto iter = m_providers.begin(); iter != m_providers.end(); ++iter) {
    *iter = 0;
  }

  m_pub_time = 0;
}

void hlt2cool::HLTRatesData::updateSim(unsigned nchains) {
  std::cout << "Updating with random data..." << std::endl;

  // Publication time of the histogram, set as current timestamp
  m_pub_time = time(0);

  // Should be done only at the first iteration
  // then next iterations should check the equivalency
  if (m_first) {
    m_chains.resize(nchains);
    std::generate(m_chains.begin(), m_chains.end(),
                  []() {return "chain" + std::to_string(std::rand());});

    // "total", "Providers" and "TriggerDB_Keys" are special chains,
    m_chains[0] = "total";
    m_chains[nchains-2] = "Providers";
    m_chains[nchains-1] = "TriggerDB_Keys";
    m_rateitems = {"input", "prescaled", "raw", "output", "rerun", "algoIn", "passedrerun"};
    createChainMap();

    m_first = 0;
  } else {
    if (m_chains.size() != nchains) {
      std::cout << "Number of chains don't fit the original list!" <<std::endl;
      exit(0);
    }
  }

  // Generate random rate data:
  m_rates.resize(nchains*m_nrateitems);
  std::generate(m_rates.begin(), m_rates.end(), []() {return 1.0 * std::rand()/RAND_MAX;});

  // Fill provider array with some data

  int randomval = (std::rand() % 5) + 5;
  m_providers.clear();
  m_providers.push_back(randomval * 100);
  m_providers.push_back(100);
  m_providers.push_back(10);
  m_providers.push_back(10);

  // Fill Providers part of vector
  unsigned ii = 0;
  for (auto p : m_providers) {
    m_rates[(nchains-2)*m_nrateitems+(ii++)] = p;
  }
  // Fill the rest with 0
  for (unsigned i = ii; i < m_nrateitems; i++) {
    m_rates[(nchains-2)*m_nrateitems+i] = 0.;
  }
}

std::vector<std::string> hlt2cool::HLTRatesData::getChains() {
  /* version 1.0: Format is same as XLabels member of ISS_TRP.HLT_Rate. Includes: 
   *               - Chain names, "Providers", "TriggerDB_Keys"
   *               (If there are N chains, there are N+2 elements)
   * version 2.0: The output vector includes:
   *               - Chain names
   *               (If there are N chains, there are N elements)
   */
  if (m_dataversion == 1.0) {
    return m_chains;
  } else if (m_dataversion == 2.0) {
    auto tmpchains = m_chains;
    auto iter_providers = std::find(tmpchains.begin(), tmpchains.end(), "Providers");  // Returns iterator to location of "Providers
    tmpchains.erase(iter_providers, tmpchains.end());  // Erase elements starting from Providers
    return tmpchains;
  } else {
    std::string s = "Unknown Data Version number: " + std::to_string(m_dataversion) + ", expected 1.0 or 2.0";
    hlt2cool::IS_Issue issue(ERS_HERE, s.c_str());
    ers::fatal(issue);
    exit(0);
  }
}

std::vector<float> hlt2cool::HLTRatesData::getRates() {
  /* In all versions, the 0th element of output vector is the m_dataversion number.
   * version 1.0: Format is same as Data member of ISS_TRP.HLT_Rate. Includes:
   *               - For each chain, all 7 rate items
   *               - Number of providers (completed up to 7 elements for consistency)
   *               - Trigger key (completed up to 7 elements for consistency)
   *               (If there are N chains, there are 1+(N+2)*7 elements)
   * version 2.0: The output vector includes:
   *               - Only output rate of each chain
   *               - Number of providers at each level
   *               (If there are N chains, P providers, there are 1+N+P elements)
   */

  if (m_dataversion == 1.0) {
    auto tmprates = m_rates;
    tmprates.insert(tmprates.begin(), m_dataversion);
    return tmprates;
  } else if (m_dataversion == 2.0) {
    std::vector<float> tmprates;
    auto iter_providers = std::find(m_chains.begin(), m_chains.end(), "Providers");  // Returns iterator to location of "Providers
    unsigned nchains = std::distance(m_chains.begin(), iter_providers);
    tmprates.reserve(nchains+8);  // 1 extra for version number, 7 extra for annotations
    tmprates.push_back(m_dataversion);

    // add only "output" rate to the final vector
    for (unsigned i = 0; i < nchains; i++) {
      tmprates.push_back(m_rates[i*m_nrateitems + 3]);
    }
    // add annotations to the final vector
    std::copy(m_providers.begin(), m_providers.end(), back_inserter(tmprates));
    return tmprates;
  } else {
    std::string s = "Unknown Data Version number: " + std::to_string(m_dataversion) + ", expected 1.0 or 2.0";
    hlt2cool::IS_Issue issue(ERS_HERE, s.c_str());
    ers::fatal(issue);
    exit(0);
  }
}

/*
void hlt2cool::HLTRatesData::PrintChainRates(std::string chainname) {
  int chain_id = chain_map[chainname];
  PrintChainRates(chain_id);
}

void hlt2cool::HLTRatesData::PrintChainRates(int chain_id) {
  std::cout << "Chain " << chain_id << " : " << chains[chain_id] << " : " << std::endl;
  for (unsigned i = 0; i < m_nrateitems; i++) {
   // std::cout << rates[i] << "=" << h.get_bin_value(chain_id+1, i+1,0) << " =? "
   std::cout << m_rates[m_nrateitems*chain_id + i ] << std::endl;
  }
}

*/

namespace hlt2cool {
  std::ostream & operator<<(std::ostream & os, const HLTRatesData & d) {
    std::cout << "publication time: " << d.m_pub_time;
    std::cout << ", Nchains, Nrateitems: " << d.m_chains.size() << "," << d.m_rateitems.size() << std::endl;
    std::cout << "XLabels(Chain Names)(first 3, last 3) : ";
    std::for_each(d.m_chains.begin(), d.m_chains.begin()+3,
                            [](const std::string & s) {std::cout << s << ", ";});
    std::cout << " ... ";
    std::for_each(d.m_chains.end()-3, d.m_chains.end(),
                            [](const std::string & s) {std::cout << s << ", ";});
    std::cout << std::endl;
    std::cout << "YLabels(Rate items) : ";
    std::for_each(d.m_rateitems.begin(), d.m_rateitems.end(),
                            [](const std::string & s) {std::cout << s << ", ";});
    std::cout << std::endl;
    std::cout << "Providers: ";
    std::for_each(d.m_providers.begin(), d.m_providers.end(),
                            [](int f) {std::cout << f << ", ";});
    std::cout << std::endl;
    std::cout << "Rates: (first 3, last 3 chains) : ";
    std::for_each(d.m_rates.begin(), d.m_rates.begin() + 3 *d.m_rateitems.size(),
                            [](float f) {std::cout << f << ", ";});
    std::cout << " ... ";
    std::for_each(d.m_rates.end()-3*d.m_rateitems.size(), d.m_rates.end(),
                            [](float f) {std::cout << f << ", ";});
    std::cout << std::endl;
    std::cout << std::endl;
    return os;
  }

  HLTRatesData operator/(HLTRatesData lhs, int N) {
    for (unsigned i = 0; i < lhs.m_providers.size(); i++) {
      lhs.m_providers[i] = lhs.m_providers[i]/N;
    }

    for (unsigned i = 0; i < lhs.m_rates.size(); i++) {
      lhs.m_rates[i] = lhs.m_rates[i]/N;
    }

    lhs.m_pub_time = lhs.m_pub_time/N;

    return lhs;
  }

  HLTRatesData operator+(HLTRatesData lhs, const HLTRatesData& rhs) {
    for (unsigned i = 0; i < lhs.m_providers.size(); i++) {
      lhs.m_providers[i] += rhs.m_providers[i];
    }

    for (unsigned i = 0; i < lhs.m_rates.size(); i++) {
      lhs.m_rates[i] += rhs.m_rates[i];
    }

    lhs.m_pub_time += rhs.m_pub_time;

    return lhs;
  }

  HLTRatesData operator*(const float weight, HLTRatesData data) {
    // Don't multiply elements of the vector after providers
    unsigned index = data.m_chain_map["Providers"];
    for (unsigned i = 0; i < index * data.m_nrateitems; i++) {
      data.m_rates[i] = weight * data.m_rates[i];
    }

    return data;
  }
}  // namespace hlt2cool

// Implicit copy assignment operator works, not necessary to define yours
// HLTRatesData & hlt2cool::HLTRatesData::operator=(const HLTRatesData & d) { std::cout << "Copy assignment constructor" <<std::endl; }
