// Program to create necessary COOL folders in an sqlite file.
// Note that the main app already does it if the folder doesn't exist
// This program is used to create a reference sqlite file before creation of folders in COOLONL
// ./create_folders -d sqlite://;schema=hltrates2cool.db;dbname=CONDBR2 -R /TRIGGER/HLT/Rates -C /TRIGGER/HLT/Chains


#include <time.h>

#include <fstream>
#include <functional>
#include <iostream>
#include <algorithm>
#include <regex>

#include "ipc/core.h"
#include "ipc/partition.h"
#include "cmdl/cmdargs.h"

#include "HLTRates2Cool/HLTRatesCoolUtils.h"

#include "boost/property_tree/ptree.hpp"

int main(int argc, char **argv) {
  // Initialise communication library
  IPCCore::init(argc, argv);


  CmdArgStr dbconnect('d', "dbcon", "dbconnect", "Database connection string");
  CmdArgStr ratesfolder('R', "ratesfolder", "ratesfolder", "Chains folder cool tag");
  CmdArgStr chainsfolder('C', "chainsfolder", "chainsfolder", "Rates folder cool tag");
  CmdArgStr ratestag('r', "ratestag", "ratestag", "Chains folder cool tag");
  CmdArgStr chainstag('c', "chainstag", "chainstag", "Rates folder cool tag");
  CmdLine cmd(*argv, &dbconnect, &ratesfolder, &chainsfolder, &ratestag, &chainstag, NULL);

  ratesfolder = "/TRIGGER/HLT/Rates";
  chainsfolder = "/TRIGGER/HLT/Chains";
  chainstag = "TriggerHltChains-Physics-01";
  ratestag = "TriggerHltRates-Physics-01";
  dbconnect = "sqlite://;schema=hltrates2cool.db;dbname=CONDBR2";

  CmdArgvIter arg_iter(--argc, ++argv);

  cmd.description("Create empty folders for HLTRates2Cool project");
  cmd.parse(arg_iter);

  std::cout <<  dbconnect << std::endl;
  std::cout <<  ratesfolder << std::endl;
  std::cout <<  chainsfolder << std::endl;
  std::cout <<  ratestag << std::endl;
  std::cout <<  chainstag << std::endl;

  // Get filename (to check if file exists)
  std::regex filename_pattern {R"(sqlite://;schema=(.*);dbname)"};  // (.*) corresponds to the filename

  std::smatch matches;  // matched strings will go in smatch
  std::string dbconnectstr = static_cast<std::string>(dbconnect); // gcc forbids temporary strings
  if (regex_search(dbconnectstr, matches, filename_pattern)) {  // search for pattern
    std::cout << "Filename: " << matches[1] << '\n';
  } else {
    std::cout << "Database connection string is malformed!" << std::endl;
    return 1;
  }

  std::string filename = matches[1];

  std::ifstream ifile(filename.c_str());
  if (static_cast<bool>(ifile)) {
    std::cout << "File " << filename << " already exists, exiting..." << std::endl;
    return 1;
  }

  // Constructor will automatically create the folder if it doesn't exist
  hlt2cool::HLTRatesCoolUtils hcool {std::string(dbconnect),
                    std::string(ratesfolder), std::string(chainsfolder),
                    std::string(ratestag), std::string(chainstag)};

  return 0;
}
