// Application to read back rates from COOL
// You need to set CORAL_DBLOOKUP_PATH and CORAL_AUTH_PATH before running this.
// Setting Athena environment will set these variables correctly
// ./read_hlt_rates -R runnumber -r ratetag -c chainstag -d dbconnect
// ./read_hlt_rates -c TestTagChain01 -r TestTagRate01 -R 317158
#include <time.h>

#include <iostream>
#include <exception>
#include <algorithm>
#include <unordered_map>
#include <ctime>

#include <boost/program_options.hpp>

#include "CoolKernel/DatabaseId.h"
#include "CoolKernel/IDatabaseSvc.h"
#include "CoolKernel/IDatabase.h"
//#include "CoolKernel/Record.h"
//#include "CoolKernel/RecordSpecification.h"
//#include "CoolKernel/FolderSpecification.h"
#include "CoolApplication/Application.h"
#include "CoolKernel/ValidityKey.h"
#include "CoolKernel/IFolder.h"
#include "CoolKernel/Exception.h"
#include "CoolKernel/IObject.h"
#include "CoolKernel/IObjectIterator.h"
#include "CoolKernel/pointers.h"
#include "CoralKernel/Context.h"

int main(int argc, char **argv) {
  namespace po = boost::program_options;

  uint64_t runno;
  std::string ratestag;
  std::string chainstag;
  std::string chaintosearch;
  std::string dbconnect;

  po::options_description desc("Allowed options");
  desc.add_options()
  ("help,h", "produce help message")
  ("runno,R", po::value<uint64_t>(&runno)->default_value(333487), "Run number")
  ("dbconnect,d", po::value<std::string>(&dbconnect)->default_value("COOLONL_TRIGGER/CONDBR2"), "COOL database connection string")
  ("ratestag,r", po::value<std::string>(&ratestag)->default_value("TriggerHltRates-Physics-01"), "COOL tag of the rates folder")
  ("chainstag,c", po::value<std::string>(&chainstag)->default_value("TriggerHltChains-Physics-01"), "COOL tag of the chains folder")
  ("chainname,C", po::value<std::string>(&chaintosearch)->default_value("total"), "Chain name");

  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);

  if (vm.count("help")) {
    std::cout << desc << "\n";
    return 1;
  }

  std::cout <<  "Run No:                    " << runno << std::endl;
  std::cout <<  "DB Connection string:      " << dbconnect << std::endl;
  std::cout <<  "COOL Tag of Rate folder:   " << ratestag << std::endl;
  std::cout <<  "COOL Tag of Chain folder:  " << chainstag << std::endl;


  const cool::ChannelId coolchannel = 0;  // Everything we use is single channel

  static cool::Application app;
  cool::IDatabaseSvc& dbSvc = app.databaseService();
  cool::IDatabasePtr dbPtr;

  // Step 1. Getting Start of RUN (Sor) and End of Run (EOR) times
  uint64_t sortime, eortime;
  {
    std::string sorfoldername ="/TDAQ/RunCtrl/SOR";
    std::string eorfoldername ="/TDAQ/RunCtrl/EOR";
    std::string dbconnect_tdaq = "COOLONL_TDAQ/CONDBR2";
    cool::IFolderPtr sorfolder;  ///< Pointer to cool folder of hlt rates
    cool::IFolderPtr eorfolder;  ///< Pointer to cool folder of hlt rates

    bool readOnly = true;

    // Open the database
    try {
      dbPtr = dbSvc.openDatabase(dbconnect_tdaq, readOnly);
    } catch (cool::DatabaseDoesNotExist& e) {
      std::cout << "Can't open TDAQ database, COOL not accessible or string is wrong!" <<std::endl;
      std::cout << "DB String: " << dbconnect_tdaq << std::endl;
      exit(0);
    } catch (std::exception &e) {
      std::cout << "Unknown exception trying to open TDAQ database "<< std::endl;
      std::cout << e.what() << std::endl;
      std::cout << "DB String: " << dbconnect_tdaq << std::endl;
      exit(0);
    }

    /* auto nodes = dbPtr->listAllNodes();
    std::cout << "Nodes:" << std::endl;
    std::cout << nodes.size() << std::endl;
    for (auto n : nodes) {
      std::cout << n << std::endl;
    } */

    // Get the SOR/EOR folders
    try {
      sorfolder = dbPtr->getFolder(sorfoldername);
      eorfolder = dbPtr->getFolder(eorfoldername);
      if (!sorfolder) {std::cout << "No soup for you!" << std::endl;}
    } catch ( cool::FolderNotFound & e) {
      std::cout << "COOL Folder: " << sorfoldername << " or " <<  eorfoldername << " doesn't exist!" << std::endl;
      exit(0);
    } catch (std::exception & e) {
      std::cout << "Unknown exception trying to get TDAQ SOR/EOR folder" << std::endl;
      std::cout << e.what() << std::endl;
      exit(0);
    }

    int lb = 2;
    const cool::ValidityKey runlb = (runno << 32) + lb;

    cool::IObjectPtr sorobject, eorobject;

    // Find SOR entry at a LB=2 (IOV in SOR folder is from LB=0 until next run)
    try {
      sorobject = sorfolder->findObject(runlb, coolchannel);
    } catch (std::exception &e) {
      std::cout << e.what() << std::endl;
      std::cout << "Can't get SOR object at run, lumi= " << runno << ", " << lb << std::endl;
      std::cout << "Either Run number is wrong, or SOR folder wasn't written in the start of the run " << std::endl;
      exit(0);
    }

    // std::cout << sorobject << std::endl;

    sortime =  sorobject->payloadValue<cool::UInt63>("SORTime");  // converting ns into s
    sortime = sortime / 1E9;

    // Find EOR entry at a LB=1 (IOV in EOR folder is from LB=0 until next run)
    try {
      eorobject = eorfolder->findObject(runlb, coolchannel);
      eortime = eorobject->payloadValue<cool::UInt63>("EORTime")/1e9;  // converting ns into s
    } catch (std::exception &e) {
      std::cout << "Can't find EOR object, run may be ongoing. Setting EOR to current time" << std::endl;
      eortime = time(0);
      std::cout << e.what() << std::endl;
    }
  }
  std::cout << "SOR, EOR: " << sortime << ", " << eortime << std::endl;

  // Step 2. Getting Chains array and rates array
  std::string chainfoldername ="/TRIGGER/HLT/Chains";
  std::string ratefoldername ="/TRIGGER/HLT/Rates";

  cool::IFolderPtr chainfolder;  ///< Pointer to cool folder of hlt rates
  cool::IFolderPtr ratefolder;  ///< Pointer to cool folder of hlt rates

  bool readOnly = true;

  // Open the database
  try {
    dbPtr = dbSvc.openDatabase(dbconnect, readOnly);
  } catch (cool::DatabaseDoesNotExist& e) {
    std::cout << "Can't open TRIGGER database, COOL not accessible or string is wrong!" <<std::endl;
    std::cout << "DB String: " << dbconnect << std::endl;
    exit(0);
  } catch (std::exception &e) {
    std::cout << "Unknown exception trying to open TRIGGER database "<< std::endl;
    std::cout << e.what() << std::endl;
    std::cout << "DB String: " << dbconnect << std::endl;
    exit(0);
  }

  // Get the Rate/Chain folders
  try {
    ratefolder = dbPtr->getFolder(ratefoldername);
    chainfolder = dbPtr->getFolder(chainfoldername);
    if (!ratefolder) {std::cout << "No soup for you!" << std::endl;}
  } catch ( cool::FolderNotFound & e) {
    std::cout << "COOL Folder: " << ratefoldername << " or " <<  chainfoldername << " doesn't exist!" << std::endl;
    exit(0);
  } catch (std::exception & e) {
    std::cout << "Unknown exception trying to get rates/chains folder" << std::endl;
    std::cout << e.what() << std::endl;
    exit(0);
  }

  // Run-LB for the chains folder
  int lb = 2;
  const cool::ValidityKey runlb = (runno << 32) + lb;

  // Start end IOV for rates folder
  const cool::ValidityKey iov_start = sortime;
  const cool::ValidityKey iov_end = eortime;

  cool::IObjectPtr chainobject;  // Single chain folder object
  cool::IObjectIteratorPtr rateobjects;  // You need to get an iterator as there are many rate objects during a run.

  // Find chain names entry at a LB (IOV in Chains folder is from LB=1 until LB=1 of next run)
  try {
    chainobject = chainfolder->findObject(runlb, coolchannel, chainstag);
  } catch (std::exception &e) {
    std::cout << "Can't get chain object at run, lumi= " << runno << ", " << lb << std::endl;
    std::cout << e.what() << std::endl;
  }

  // Check if the run number is correct
  auto since = chainobject->since();
  if (since >> 32 != runno) {
    std::cout << "Run number of the chain object doesn't fit!. Expected: " << runno << ", Read: " << (since>>32) << std::endl;
    exit(0);
  }

  std::string chainnames = chainobject->payloadValue<std::string>("chains");
  // std::cout << chainnames << std::endl;

  // Hash map of chain names and indices
  std::unordered_map<std::string, unsigned> chainmap;

  // Vector of chains, in case you want to get chain name from index
  std::vector<std::string> chainvector;

  // Go through comma seperated chain names and add them to the unordered_map
  std::stringstream ss(chainnames);
  std::string temp2;
  unsigned i = 0;
  while (std::getline(ss, temp2, ',')) {
      chainmap[temp2] = i;
      chainvector.push_back(temp2);
      i++;
  }

  unsigned nchains = chainvector.size();  // Total number of chains
  std::cout << "Number of chains: " << nchains << std::endl;

  try {
    rateobjects = ratefolder->browseObjects(iov_start, iov_end, coolchannel, ratestag);
  } catch (std::exception &e) {
    std::cout << "Can't get rate object between "<< iov_start << " - " << iov_end << std::endl;
    std::cout << e.what() << std::endl;
  }

  // Vector of vectors (inner vector has rates of one IOV)
  std::vector<std::vector<float>> ratesvector;

  // Vector of since/until values
  std::vector<std::pair<int, int>> sinceuntil;

  // Main loop that goes through IOVs and gets rate vectors
  while (rateobjects->goToNext()) {
    const cool::IObject & currentobj = rateobjects->currentRef();

    // IOV of the current object
    auto since = currentobj.since();
    auto until = currentobj.until();

    // If the folder is written correctly, all the since/until should be in between iov_start-iov_end.
    // However if items were written with open ended iovs for any reason, their "since" will still be
    // within next run's iov_start-iov_end. Discarding such entries
    if (since < iov_start || until > iov_end) {
      std::cout << "IOV out of bounds, may be from previous/next runs!. since, until = " << since << ", " << until << std::endl;
      std::cout << std::endl;
      continue;
    }

    // Get the blob.
    const cool::Blob16M & rateblob = currentobj.payloadValue<cool::Blob16M>("rates");

    // Number of floats in the blob
    // This should be nchains + 1 (version number) + 4 (providers)
    unsigned nelements = rateblob.size()/sizeof(float);

    // Convert void* to float*. pdata points to start of float array.
    float* pdata = (float*)(rateblob.startingAddress());

    // Create vector with contents of blob
    std::vector<float> vtemp(pdata, pdata+nelements);

    // Data format is as follows for version 2.0:
    // vtemp[0]: version number
    // vtemp[i+1]: ith chains output rate
    // vtemp[nchains+1+j]: jth provider (number of summed histograms, taken from annotations of Rate20 histogram)
    //                   Normally there are 4 elements for providers
    std::cout << "since, until: " << since << ", " << until <<std::endl;
    std::cout << "Nchains: " << nchains << ", Nelements: " << nelements << std::endl;
    float version = vtemp[0];
    std::cout << "version number: " << version << std::endl;
    if (version == 2.0) {
      std::cout << "Rates of each chain(first 10 chains, last 10 chains)"  << std::endl;
      for (unsigned i = 0; i < 10; i++)
        { std::cout << chainvector[i] << ": " << vtemp[i+1] << std::endl; }
        std::cout << "..." << std::endl;
      for (unsigned i = nchains - 10;  i< nchains; i++)
        { std::cout << chainvector[i] << ": " << vtemp[i+1] << std::endl; }

      std::cout << "Providers (Number of summed histograms at each level)" << std::endl;
      for (unsigned i = nchains+1; i < vtemp.size(); i++) {
        std::cout << vtemp[i] << ", ";
      }
      std::cout << std::endl;
      std::cout << std::endl;
    }
    // Push the data of current IOV to ratesvector
    ratesvector.push_back(vtemp);
    sinceuntil.push_back(std::make_pair(since, until));
  }

  // To get rate of a specific chain, one need to do something like:
  unsigned index = chainmap[chaintosearch];
  std::cout << "IOV and Rates for Chain: " << chaintosearch << std::endl;
  for (unsigned i = 0; i < ratesvector.size(); i++) {
    std::cout << sinceuntil[i].first << "-" << sinceuntil[i].second << " : "  ;
    std::cout << ratesvector[i][index+1] << std::endl;
  }
  std::cout << std::endl;


  return 0;
}
