// Program to test writing to cool
// ./test_cool-d BLAH_ONLINE/CONDBR -c /TRIGGER/HLT/MENU -t mytag



#include <time.h>

#include <functional>
#include <iostream>
#include <exception>
#include <algorithm>

#include "ipc/core.h"
#include "ipc/partition.h"
#include "cmdl/cmdargs.h"

#include "HLTRates2Cool/HLTRatesCoolUtils.h"

#include "boost/property_tree/ptree.hpp"

int main(int argc, char **argv) {
  // Initialise communication library
  IPCCore::init(argc, argv);


  CmdArgStr dbconnect('d', "dbcon", "dbconnect", "Database connection string");
  CmdArgStr ratesfolder('R', "ratesfolder", "ratesfolder", "Chains folder cool tag");
  CmdArgStr chainsfolder('C', "chainsfolder", "chainsfolder", "Rates folder cool tag");
  CmdArgStr ratestag('r', "ratestag", "ratestag", "Chains folder cool tag");
  CmdArgStr chainstag('c', "chainstag", "chainstag", "Rates folder cool tag");
  CmdLine cmd(*argv, &dbconnect, &ratesfolder, &chainsfolder, &ratestag, &chainstag, NULL);

  // partition_name = "part_test_HLT";
  // isobjectname = "Histogramming.TopMIG-OH:HLT./SHIFT/TrigSteer_HLT/Rate20s";
  ratesfolder = "/TRIGGER/HLT/Rates";
  chainsfolder = "/TRIGGER/HLT/Menu";
  chainstag = "TriggerHltChains-Physics-01";
  ratestag = "TriggerHltRates-Physics-01";
  dbconnect = "sqlite://;schema=test.db;dbname=TESTCOOL";

  CmdArgvIter arg_iter(--argc, ++argv);

  cmd.description("test cool write");
  cmd.parse(arg_iter);

  std::cout <<  dbconnect << std::endl;
  std::cout <<  ratesfolder << std::endl;
  std::cout <<  chainsfolder << std::endl;
  std::cout <<  ratestag << std::endl;
  std::cout <<  chainstag << std::endl;

  // Use defaults!
  hlt2cool::HLTRatesCoolUtils hcool {std::string(dbconnect),
                    std::string(ratesfolder), std::string(chainsfolder),
                    std::string(ratestag), std::string(chainstag)};

  // For payload style as channels:
  std::vector<float> vdata;

  unsigned nchains = 10;
  //unsigned nrates = 7;  // Number of rate items, needed for data version 1.0

  vdata.resize(nchains);

  uint64_t since;

  // Create random names for chains
  std::vector<std::string> vchains(nchains);
  std::generate(vchains.begin(), vchains.end(), []() {return "chain" + std::to_string(std::rand());});

  uint64_t runno = 301559;

  // Write chains to chain folder
  try {
    hcool.writeChains(vchains, runno);
  } catch (std::exception & e ) {
    std::cout << "writing Chains failed due to: " << e.what() << std::endl;
  }

  for (unsigned i = 0; i < 10; i++) {
    since = time(0);
    std::generate(vdata.begin(), vdata.end(), []() {return 10000000./std::rand();});
    try {
      hcool.writeRates(vdata, since);
    } catch (std::exception & e ) {
      std::cout << "writing rates failed due to: " << e.what() <<std::endl;
    }
    sleep(1);
  }
  return 0;
}
