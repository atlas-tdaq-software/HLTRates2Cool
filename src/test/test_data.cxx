// Testing operations on HLTRatesData class
// ./test_data
// author: cenk.yildiz@cern.ch

#include <time.h>

// #include <functional>
#include <iostream>

// #include "boost/property_tree/ptree.hpp"

#include "HLTRates2Cool/HLTRatesData.h"

// typedef boost::property_tree::ptree ptree;

template <typename T>
void printvector(std::vector<T> vec) {
  for (auto p : vec) {std::cout << p <<" ";}
  std::cout << std::endl;
}


int main(int argc, char **argv) {
  float weight;

  std::vector<hlt2cool::HLTRatesData> v_data;

  hlt2cool::HLTRatesData sum_data;
  hlt2cool::HLTRatesData my_data;

  sum_data.updateSim(10);
  my_data.updateSim(10);


  // Getting the first element of providers as the reference for whole summing
  int Nprov = sum_data.getProviders()[0];

  weight = static_cast<float>(my_data.getProviders()[0])/ Nprov;

  std::cout << "first: " << std::endl;
  std::cout << sum_data << std::endl;

  std::cout << "Chains from getChains(): ";
  printvector<std::string>(sum_data.getChains());
  std::cout << "Rates from getRates(): ";
  printvector<float>(sum_data.getRates());

  std::cout << "sec: " << std::endl;
  std::cout << my_data << std::endl;

  std::cout << "weight(" << weight << ") * sec: " << std::endl;
  std::cout << weight * my_data << std::endl;

  sum_data += weight * my_data;

  std::cout << "SUM: " << std::endl;
  std::cout << sum_data << std::endl;

  std::cout << "AVG: " << std::endl;
  std::cout << sum_data/2 << std::endl;

  my_data.updateSim(10);
  std::cout << my_data << std::endl;

  std::cout << "SUM: " << std::endl;
  sum_data = sum_data + my_data;
  auto avg_data = sum_data / 3;
  std::cout << avg_data << std::endl;

  std::cout << "Chains from getChains(): ";
  printvector<std::string>(avg_data.getChains());
  std::cout << "Rates from getRates(): ";
  printvector<float>(avg_data.getRates());

  sum_data.updateSim(10);
  my_data.updateSim(10);

  std::cout << "first : " << std::endl;
  std::cout << sum_data << std::endl;

  std::cout << "sec : " << std::endl;
  std::cout << my_data << std::endl;

  std::cout << "first * 0.5: " << std::endl;
  std::cout << (sum_data*0.5) << std::endl;

  std::cout << "sec *1.2: " << std::endl;
  std::cout << (my_data*1.2) << std::endl;

  std::cout << "SUM" << std::endl;
  std::cout << (sum_data*0.5 + my_data*1.2) << std::endl;

  std::cout << "AVG" << std::endl;
  std::cout << (sum_data*0.5 + my_data*1.2) /2 << std::endl;

  sum_data.reset();
  my_data.updateSim(10);

  std::cout << "reset data: " << std::endl;
  std::cout << sum_data  << std::endl;

  std::cout << "sec *1.2: " << std::endl;
  std::cout << (my_data*1.2) << std::endl;

  std::cout << "SUM" << std::endl;
  std::cout << (sum_data + my_data*1.2) << std::endl;

  return 0;
}
