#include <signal.h>
#include <ctime>

#include "ers/ers.h"

#include "HLTRates2Cool/HLTRates2Cool.h"


namespace hlt2cool {

void HLTRates2Cool::signalHandler(int signum) {
  std::cout << "Interrupt signal (" << signum << ") received." << std::endl;
  HLTRates2Cool::m_signalreceived = 1;
}

HLTRates2Cool::HLTRates2Cool(const IPCPartition & partition, const std::string isobject,
                             const std::string dbcon,
                             const std::string ratesfolder, const std::string chainsfolder,
                             const std::string ratestag, const std::string chainstag,
                             const unsigned pubint, const int maxpubs,  const bool labmode):
                              m_partition {partition}, m_isobject {isobject},
                              m_rec {new ISInfoReceiver(m_partition)},
                              m_cool {new HLTRatesCoolUtils(dbcon, ratesfolder, chainsfolder, ratestag, chainstag)},
                              m_pubinterval {pubint}, m_maxpubs {maxpubs}, m_labmode {labmode}
                              {
  m_isInfoDict = ISInfoDictionary(m_partition);
  std::cout << "Configuration for HLTRates2Cool:" <<std::endl;
  std::cout << "================================" <<std::endl;
  std::cout << "m_isobject  :" << m_isobject      <<std::endl;
  std::cout << "dbcon       :" << dbcon         <<std::endl;
  std::cout << "ratesfolder :" << ratesfolder     <<std::endl;
  std::cout << "chainsfolder:" << chainsfolder    <<std::endl;
  std::cout << "ratestag    :" << ratestag        <<std::endl;
  std::cout << "chainstag   :" << chainstag       <<std::endl;
  std::cout << "pubint      :" << pubint        <<std::endl;
  std::cout << "maxpubs     :" << maxpubs       <<std::endl;
  std::cout << "labmode     :" << labmode       <<std::endl;
  std::cout << std::endl;

  // One can remove m_rec and m_cool from initializer list and change to for  c++14
  // m_rec = std::make_unique<ISInfoReceiver>(m_partition);
  // m_cool = std::make_unique<HLTRatesCoolUtils>(m_dbconnect);

  signal(SIGABRT, HLTRates2Cool::signalHandler);
  signal(SIGTERM, HLTRates2Cool::signalHandler);
  signal(SIGINT,  HLTRates2Cool::signalHandler);

  m_app_start_time = time(0);
  ERS_LOG("Started app " << m_app_start_time);

  // Just send a warning if running in lab mode
  if (m_labmode) {
    ERS_INFO("Running in Lab Mode, Stable beams/ready4physics check will be replaced with if LB >2");
  }
}

void HLTRates2Cool::subscribe() {
  ERS_INFO("Subscribing to IS Object: " << m_isobject);

  try {
    m_rec->subscribe(m_isobject, &HLTRates2Cool::callback, this);
    m_subscribed = 1;
  } catch (...) {
    std::string s = "Can't subscribe to " + m_isobject;
    hlt2cool::HLTRates2Cool_Issue issue(ERS_HERE, s.c_str());
    ers::error(issue);
  }
}

void HLTRates2Cool::unsubscribe() {
  if (!m_subscribed) {
    ERS_INFO("Already unsubscribed from IS Object: " << m_isobject);
    return;
  }

  ERS_INFO("Unsubscribing from IS Object: " << m_isobject);

  try {
    m_rec->unsubscribe(m_isobject);
    m_subscribed = 0;
  } catch (...) {
    std::string s = "Can't unsubscribe from " + m_isobject;
    hlt2cool::HLTRates2Cool_Issue issue(ERS_HERE, s.c_str());
    ers::error(issue);
  }
}

void HLTRates2Cool::run() {
  // Continue until received a signal
  while (HLTRates2Cool::m_signalreceived == 0) {
    sleep(2);
  }
  if (HLTRates2Cool::m_signalreceived) {
    ERS_INFO("Signal Received, leaving run");

    if (m_nsummed) {  // If there is data waiting to be written to COOL (Can happen if run stopped during stable beams)
      m_sum_data = m_sum_data / m_nsummed;

      std::vector<float> m_rates = m_sum_data.getRates();

      ERS_LOG("Data to write to COOL:");
      std::cout <<  m_sum_data << "\n" << std::endl;

      uint64_t current_time = time(0);
      try {
        // Set COOL IOV as (last COOL publication time, current time)
        m_cool->writeRates(m_rates, m_coolpubtime*1E9, current_time*1E9); // Convert to ns
      } catch(std::exception & e) {
        ERS_LOG("Can't write to COOL due to: '" << e.what() << "'. Data will be buffered.");
        dataqueue.push(std::make_tuple(m_rates, m_coolpubtime*1E9, current_time*1E9));
      }
    }

    writeBuffer2Cool();  // Empty buffer into cool or fallback sqlite file, if still not empty
  }
}

bool HLTRates2Cool::writeBuffer2Cool() {
    if (dataqueue.empty()) { return true;}

    unsigned nwritten = 0;
    while ( !dataqueue.empty() ) {
      try {
        auto t = dataqueue.front();  // get first element
        m_cool->writeRates(std::get<0>(t), std::get<1>(t), std::get<2>(t));
        dataqueue.pop();
        nwritten++;
      } catch (std::exception & e) {
        std::cout << "Can't write to COOL due to: '" << e.what() << std::endl;
        std::string s = std::to_string(nwritten) + " elements written from buffer to COOL, " +
                        std::to_string(dataqueue.size()) + " still in buffer. They will be retried next time";
        hlt2cool::HLTRates2Cool_Issue issue(ERS_HERE, s.c_str());
        ers::warning(issue);

        // empty buffer into sqlite file if it has grown too large or signal received
        if ((dataqueue.size() > m_maximum_buffer_size || m_signalreceived)) {
            fallback_sqlite_connection = "sqlite://;schema=/det/tdaq/cyildiz/hltrates2cool_" + std::to_string(m_runno) + ".db;dbname=CONDBR2";
            if (m_labmode) {
              fallback_sqlite_connection = "sqlite://;schema=/tmp/hltrates2cool_" + std::to_string(m_runno) + ".db;dbname=CONDBR2";
            }
            std::string s = "Buffer has more than " + std::to_string(m_maximum_buffer_size)
                            + " entries or signal received. Writing rest of the entries to sqlite db: " + fallback_sqlite_connection;
            hlt2cool::HLTRates2Cool_Issue issue(ERS_HERE, s.c_str());
            ers::warning(issue);
            HLTRatesCoolUtils fallback(fallback_sqlite_connection,
                                        m_cool->getratesfoldername(), m_cool->getchainsfoldername(),
                                        m_cool->getratestag(), m_cool->getchainstag());

            // Write all data in the buffer to the fallback sqlite file
            while ( !dataqueue.empty() ) {
              try {
                auto t = dataqueue.front();  // get first element
                fallback.writeRates(std::get<0>(t), std::get<1>(t), std::get<2>(t));
                dataqueue.pop();
              } catch (...) {
                std::string s = "Writing buffer into sqlite db also failed, " + std::to_string(dataqueue.size() > 0) + " elements still in buffer, removing the oldest entry.";
                hlt2cool::HLTRates2Cool_Issue issue(ERS_HERE, s.c_str());
                ers::fatal(issue);
                dataqueue.pop();  // Remove one entry so buffer size doesn't grow indefinitely
                return false;
              }
            }

            // If chains are still not written to COOL when signal is received, write them to fallback sqlite file
            if (m_signalreceived && !m_chainsWrittenToCool) {
              ERS_INFO("Signal received and chain names still not stored in COOL! Writing into sqlite db: " << fallback_sqlite_connection);
              std::vector<std::string> vchains = m_sum_data.getChains();
              fallback.writeChains(vchains, m_runno);
            }
            return true;
        }

        return false;  // Buffer still not empty
      }
    }
    ERS_INFO(nwritten << " elements written from buffer to COOL, buffer empty");
    return true;  // buffer empty
}

void HLTRates2Cool::getRunLb() {
  LumiBlock lb;
  ERS_DEBUG(0, "Getting LumiBlock info");
  m_isInfoDict.getValue("RunParams.LumiBlock", lb);
  m_runno = lb.RunNumber;
  m_lb = lb.LumiBlockNumber;
  ERS_DEBUG(0, "run, lb = " << m_runno << ", "  << m_lb);
}

bool HLTRates2Cool::getReady4Physics() {
  Ready4PhysicsInfo r4physics;
  m_isInfoDict.getValue("RunParams.Ready4Physics", r4physics);
  return r4physics.ready4physics;
}

uint64_t HLTRates2Cool::getReady4PhysicsTime() {
  Ready4PhysicsInfo r4physics;
  m_isInfoDict.getValue("RunParams.Ready4Physics", r4physics);
  OWLTime t_owl = r4physics.time();

  return t_owl.total_mksec_utc()/1E6;  // Convert usec to sec since epoch
}

uint64_t HLTRates2Cool::getSORTime() {
  RunParams sor_params;
  m_isInfoDict.getValue("RunParams.SOR_RunParams", sor_params);
  OWLTime t_owl = sor_params.timeSOR;

  return t_owl.total_mksec_utc()/1E6;  // Convert usec to sec since epoch
}

void HLTRates2Cool::callback(ISCallbackInfo * isc) {
  ERS_DEBUG(0, "callback: " << isc->name() << ", reason code: " << isc->reason());
  bool stable;  // ready4physics flag, which is set after stable beams
  uint64_t is_pub_time;  // publication time of is_object
  float weight = 1.0;  // weight to be used in weighted sum

  HLTRates2Cool * h = static_cast<HLTRates2Cool*>(isc->parameter());

  // Prevent a second callback to run at the same time
  std::lock_guard<std::mutex> lock(h->m_mutex);
  ERS_DEBUG(0, "Mutex locked for callback actions");

  // Setting reference to 2 HLTRatesData instances as they
  // are used a lot in this method
  HLTRatesData & m_current_data = h->m_current_data;
  HLTRatesData & m_sum_data = h->m_sum_data;

  try {
    h->getRunLb();
  } catch(std::exception & e) {
    std::string s = "Cannot access RunParams.LumiBlock info: '" + std::string(e.what());
    hlt2cool::HLTRates2Cool_Issue issue(ERS_HERE, s.c_str());
    ers::error(issue);
    return;
  }

  // Getting the ready4physics value
  // Replace ready4physics flag with lumiblock check in lab mode
  if (m_labmode) {
    if (h->m_lb > 2)
      stable = 1;
    else
      stable = 0;
  } else {
    try {
      stable = h->getReady4Physics();
    } catch(std::exception & e) {
      std::string s = "Cannot access RunParams.Ready4Physics information: '" + std::string(e.what());
      hlt2cool::HLTRates2Cool_Issue issue(ERS_HERE, s.c_str());
      ers::error(issue);
      return;
    }
  }

  // Don't do anything if there isn't stable beams/ready4physics in current and previous callback
  if (!stable && !h->m_stable) {
    ERS_DEBUG(0, "Stable beams/ready4physics not reached, doing nothing ...");
    return;
  }

  ERS_DEBUG(0, "ready4physics is on...");

  // If we switch from non stable beams to stable beams situation
  if (!h->m_stable && stable) {
    ERS_LOG("Switching to stable beams/ready4physics mode ...");
    m_sum_data.update(isc);  // Initialize m_sum_data with data from current callback

    // Make the next IOV from start of ready4physics-1 (-1 for making sure all events are included)
    // If ready4physics was updated before app started, use start time as the iov start
    auto r4p_time = h->getReady4PhysicsTime()-1;
    ERS_DEBUG(2, "app started at: " << h->m_app_start_time << ", ready4physics time: " << r4p_time);
    if (m_app_start_time > r4p_time) {
      h->m_coolpubtime = h->m_app_start_time;
    } else {
      h->m_coolpubtime = r4p_time;
    }

    if (h->m_first) {
      h->m_first = 0;

      // Get number of lowest level of providers as reference value for
      // number of providers
      h->m_nprov = m_sum_data.getProviders()[0];

      ERS_LOG("First callback with stable beams/ready4physics. Chains:");
      std::vector<std::string> vchains = m_sum_data.getChains();
      for (auto c : vchains) {
        std::cout << c <<",";
      }
      std::cout << std::endl;
    }
    h->m_stable = stable;
    h->m_nsummed++;

    return;  // sum_data is already updated with current is info
  }

  m_current_data.update(isc);

  is_pub_time = m_current_data.getPublicationTime();

  // Weight is inversely proportional to the number of providers.
  // While using ISS_TRP, one doesn't need it as it's already rescaled
  if (isc->type().name() == "HistogramData<float>") {
    weight = static_cast<float>(h->m_nprov)/ m_current_data.getProviders()[0];
    ERS_DEBUG(1, "weight = prov0/prov_now: " << h->m_nprov  <<  " / " << m_current_data.getProviders()[0] << " = " << weight);
  } else if (isc->type().name() == "TimePoint_IS") {
    weight = 1.0;
  }

  // Update the sum only if we're still at stable beams
  if (stable) {
    ERS_DEBUG(1, "Previous sum:");
    ERS_DEBUG(1, m_sum_data << "\n");
    ERS_DEBUG(1, "Current data:");
    ERS_DEBUG(1, m_current_data << "\n");
    ERS_DEBUG(1, "Current data * weight(" << weight << "):");
    ERS_DEBUG(1, weight * m_current_data << "\n");

    m_sum_data += weight * m_current_data;
    h->m_nsummed++;  // Keep track of number of summed elements
    ERS_DEBUG(1, "New sum:");
    ERS_DEBUG(1, m_sum_data << "\n");

    ERS_DEBUG(1, "is time, cool time: " << is_pub_time << ", " << h->m_coolpubtime);
  }

  // Do a new cool publication if time difference is bigger than m_pubinterval or we are not in stable beams anymore
  if ((is_pub_time - h->m_coolpubtime >= h->m_pubinterval) || (!stable && h->m_stable && h->m_nsummed)) {
    if (!h->m_chainsWrittenToCool) {
      try {
        std::vector<std::string> vchains = m_sum_data.getChains();
        h->m_cool->writeChains(vchains, h->m_runno);
        h->m_chainsWrittenToCool = true;
      } catch(std::exception & e) {
        std::string s = "Can't write Chains into COOL due to: '" + std::string(e.what()) + "', will try later";
        hlt2cool::HLTRates2Cool_Issue issue(ERS_HERE, s.c_str());
        ers::warning(issue);
      }
    }

    // Take average of the weighted sum
    m_sum_data = m_sum_data / h->m_nsummed;

    std::vector<float> m_rates = m_sum_data.getRates();

    ERS_LOG("Data to write to COOL:");
    std::cout <<  m_sum_data << "\n" << std::endl;

    if (h->writeBuffer2Cool() == true) {  // Is buffer empty
      try {
        // Set COOL IOV as (last COOL publication time, current IS publication time)
        h->m_cool->writeRates(m_rates, h->m_coolpubtime*1E9, is_pub_time*1E9); // convert to ns
      } catch(std::exception & e) {
        ERS_LOG("Can't write to COOL due to: '" << e.what() << "'. Data will be buffered.");
        h->dataqueue.push(std::make_tuple(m_rates, h->m_coolpubtime*1E9, is_pub_time*1E9));
      }
    } else {
      ERS_LOG("Buffer not empty, current data will be added to buffer.");
      h->dataqueue.push(std::make_tuple(m_rates, h->m_coolpubtime*1E9, is_pub_time*1E9));
      ERS_LOG("Buffer size: " << dataqueue.size());
    }

    // Reset m_coolpubtime to current publication time
    h->m_coolpubtime = is_pub_time;

    // Empty the vector of HLTRatesData
    h->m_npubs++;
    ERS_LOG("Averaged over " << h->m_nsummed << " instances"
             << ", publication number: " << h->m_npubs);

    h->m_nsummed = 0;

    // Reset m_sum_data
    m_sum_data.reset();

    // If number of maximum cool publications is reached, unsubscribe
    // if m_maxpubs == -1 data will be written as long as instance is alive
    if (h->m_maxpubs != -1 && h->m_npubs >= h->m_maxpubs) {
      ERS_INFO("Number of maximum publications reached, unsubscribing from IS server");
      h->unsubscribe();
      return;
    }
  }

  h->m_stable = stable;  // Update previous stable beams/ready4physics status
}

HLTRates2Cool::~HLTRates2Cool() {
  ERS_DEBUG(0, "Destructor for HLTRates2Cool called.");

  // In case destructor is called without unsubscribing, try to unsubscribe
  unsubscribe();
}
}  // namespace hlt2cool
