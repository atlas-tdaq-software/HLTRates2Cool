//  author: cenk.yildiz@cern.c
//
// ./hltrates2cool_app -p part_jobOpt_jetDS_cyildiz -d sqlite://;schema=test.db;dbname=TESTCOOL -i Histogramming.TopMIG-OH:HLT./SHIFT/TrigSteer_HLT/Rate20s -d BLAH_ONLINE/CONDBR -c /TRIGGER/HLT/MENU
// ./hltrates2cool_app -p part_hlt_cyildiz -d sqlite://;schema=test.db;dbname=TESTCOOL -i Histogramming.HLTSV.ProcessingTime -d BLAH_ONLINE/CONDBR -c /TRIGGER/HLT/MENU
// ./hltrates2cool_app -p part_trp_cyildiz_20.11.2.2 -d sqlite://;schema=test.db;dbname=TESTCOOL -i Histogramming.TopMIG-OH:HLT./SHIFT/TrigSteer_HLT/Rate20s
// ./hltrates2cool_app -p part_hlt_cyildiz -d sqlite://;schema=test.db;dbname=TESTCOOL  -i RunParams.RunInfo -r RateTag -c ChainTag -I 50 -N 10
// ./hltrates2cool_app -p ${TDAQ_PARTITION} -d sqlite://;schema=test.db;dbname=TESTCOOL -i RunParams.RunInfo -I 60 -N 10
//
//  Suggested arguments for OKS:
//  -p ${TDAQ_PARTITION} -I 300 -N 5


#include "ers/ers.h"
#include <is/info.h>
#include <is/infoT.h>
#include <is/inforeceiver.h>
#include <is/infodynany.h>
#include <ipc/core.h>
#include <ipc/partition.h>
#include <cmdl/cmdargs.h>

#include <signal.h>
#include <time.h>

// #include <functional>
#include <iostream>

#include "HLTRates2Cool/HLTRates2Cool.h"

// typedef boost::property_tree::ptree ptree;

// int signalReceived = 0;

// void signal_handler(int signum) {
//   std::cout << "Interrupt signal (" << signum << ") received in main." << std::endl;
//   signalReceived = signum;
// }

//! \brief Main application for HLTRates2Cool project
/*
 * This app uses HLTRates2Cool class, and reads an IS object to get HLT_Rates
 * and writes to the dedicated COOL folders.
 * See the documentation of HLTRates2Cool class to see implementation details.
 */

int main(int argc, char **argv) {
  // Initialise communication library
  IPCCore::init(argc, argv);

  CmdArgStr partition_name('p', "partition", "partition_name", "Partition name");
  CmdArgStr isobjectname('i', "is_object", "isobjectname", "Is object name");
  CmdArgStr dbconnect('d', "dbcon", "dbconnect", "Database connection string");
  CmdArgStr ratesfolder('R', "ratesfolder", "ratesfolder", "Chains folder");
  CmdArgStr chainsfolder('C', "chainsfolder", "chainsfolder", "Rates folder");
  CmdArgStr ratestag('r', "ratestag", "ratestag", "Chains folder cool tag");
  CmdArgStr chainstag('c', "chainstag", "chainstag", "Rates folder cool tag");
  CmdArgInt pubinterval('I', "pubinterval", "pubinterval", "Cool publication interval in seconds");
  CmdArgInt maxpubs('N', "maxpubs", "maxpubs", "Number of maximum cool publications, -1 means no maximum, publication will be done until EOR");
  CmdArgBool labmode('L', "labmode", "Lab mode, the stable beams check is replaced by a lumiblock check");
  CmdLine cmd(*argv, &partition_name, &isobjectname, &dbconnect, &ratesfolder,
              &chainsfolder, &ratestag, &chainstag, &pubinterval, &maxpubs, &labmode, NULL);

  // partition_name = "ATLAS"; // No default provided, it should always be taken from environment
  isobjectname = "ISS_TRP.HLT_Rate";
  // dbconnect = "sqlite://;schema=test.db;dbname=TESTCOOL";
  pubinterval = 300;
  maxpubs = -1;

  // Following options should not be set in command line unless it's needed for test
  dbconnect = "COOLONL_TRIGGER/CONDBR2";
  ratesfolder = "/TRIGGER/HLT/Rates";
  chainsfolder = "/TRIGGER/HLT/Chains";
  chainstag = "TriggerHltChains-Physics-01";
  ratestag = "TriggerHltRates-Physics-01";

  CmdArgvIter arg_iter(--argc, ++argv);

  cmd.description("HLT Rates Cool Archiver");
  cmd.parse(arg_iter);

  if (maxpubs < -1 || maxpubs == 0) {
    std::string s = "Number of maximum publication is set to: " + std::to_string(maxpubs) + ", expected -1 or a positive integer!";
    hlt2cool::HLTRates2Cool_Issue issue(ERS_HERE, s.c_str());
    ers::fatal(issue);
    exit(1);
  }

  if (pubinterval < 10) {
    std::string s = "Publication interval set to : " + std::to_string(pubinterval) + ", expected an integer greater or equal to 10!";
    hlt2cool::HLTRates2Cool_Issue issue(ERS_HERE, s.c_str());
    ers::fatal(issue);
    exit(1);
  }

  // Signal handling done in HLTRates2Cool
  // signal(SIGABRT, signal_handler);
  // signal(SIGTERM, signal_handler);
  // signal(SIGINT,  signal_handler);

  IPCPartition part(partition_name);

  std::cout << "HLT Rates 2 Cool command line options" << std::endl;
  std::cout << "=====================================" << std::endl;
  std::cout << "partition_name : " <<  partition_name << std::endl;
  std::cout << "isobjectname   : " <<  isobjectname   << std::endl;
  std::cout << "dbconnect      : " <<  dbconnect      << std::endl;
  std::cout << "ratesfolder    : " <<  ratesfolder    << std::endl;
  std::cout << "chainsfolder   : " <<  chainsfolder   << std::endl;
  std::cout << "ratestag       : " <<  ratestag       << std::endl;
  std::cout << "chainstag      : " <<  chainstag      << std::endl;
  std::cout << "pubinterval    : " <<  pubinterval    << std::endl;
  std::cout << "maxpubs        : " <<  maxpubs        << std::endl;
  std::cout << "labmode        : " <<  labmode        << std::endl;


  std::cout << "Starting hltrates2cool_app" <<std::endl;
  hlt2cool::HLTRates2Cool h2c(part, std::string(isobjectname),
                    std::string(dbconnect),
                    std::string(ratesfolder), std::string(chainsfolder),
                    std::string(ratestag), std::string(chainstag),
                    unsigned(pubinterval), int(maxpubs), bool(labmode));

  h2c.subscribe();

  // Keep the app alive until the maxium number of cool writes reached or run stopped
  h2c.run();

  std::cout << "Unsubscribing and exiting ..." <<std::endl;
  h2c.unsubscribe();

  return 0;
}
