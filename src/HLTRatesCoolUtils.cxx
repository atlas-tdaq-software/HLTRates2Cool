// A class taking care of reading and writing from COOL.
// The offline code doesn't have access to tdaq software, can't use this class

// #include <fstream>  // Used for throwing fake exception
#include <exception>

#include <cstring>
#include <numeric>

#include "ers/ers.h"

#include "HLTRates2Cool/HLTRatesCoolUtils.h"

ERS_DECLARE_ISSUE(hlt2cool,
                  COOL_Issue,
                  "HLTRates2Cool Cool issue: " << message,
                  ((const char *)message)
                  )

hlt2cool::HLTRatesCoolUtils::HLTRatesCoolUtils(const std::string dbId,
                                               const std::string ratesfolder,
                                               const std::string chainsfolder,
                                               const std::string ratestag,
                                               const std::string chainstag)
                       : m_dbId {dbId},
                       m_ratesfolderName {ratesfolder}, m_chainsfolderName {chainsfolder},
                       m_ratestag {ratestag}, m_chainstag {chainstag} {
  std::cout << "Configuration for HLTRatesCoolUtils:" << std::endl;
  std::cout << "====================================" << std::endl;
  std::cout << "ratesfolder :" << ratesfolder  << std::endl;
  std::cout << "chainsfolder:" << chainsfolder << std::endl;
  std::cout << "ratestag    :" << ratestag     << std::endl;
  std::cout << "chainstag   :" << chainstag    << std::endl;
  std::cout << std::endl;

  setRecordSpec();
  // Opening database to create db and folders if necessary. Don't throw exceptions to HLTRates2Cool, as now we only want to inform user that something is not right.
  try {
    ERS_INFO("Opening COOL database");
    openDatabase();
    openFolder();
    m_dbPtr->closeDatabase();
  } catch (std::exception &e) {
    std::string temp = "First attempt to open database and COOL folders failed: " + std::string(e.what());
    hlt2cool::COOL_Issue issue(ERS_HERE, temp.c_str());
    ers::fatal(issue);
  }
}

void hlt2cool::HLTRatesCoolUtils::openDatabase() {
  bool readOnly = false;

  // Read from a file to explicitly throw exception (used to test buffering mechanism)
  // int coolissue;
  // std::ifstream issuefile;           // create object to read
  // issuefile.open("/tmp/cyildiz_cool");  // associate with a file
  // issuefile >> coolissue;
  // std::cout << "Issue: " << coolissue << std::endl;
  // if (coolissue && (m_dbId.find("COOLONL") != std::string::npos)) {  // Throw exception only for COOLONL, not for sqlite files
  //   throw std::runtime_error("Fake exception (Fakeception)");
  // }

  // Opening the database, or creating new sqlite db if it doesn't exist
  static cool::Application app;
  cool::IDatabaseSvc& dbSvc = app.databaseService();
  try {
    m_dbPtr = dbSvc.openDatabase(std::string(m_dbId), readOnly);
  } catch (cool::DatabaseDoesNotExist& e) {
    if (m_dbId.find("sqlite:") != std::string::npos) {
      std::string temp = "Can't open the database, creating a new one.";
      hlt2cool::COOL_Issue issue(ERS_HERE, temp.c_str());
      ers::warning(issue);
      m_dbPtr = dbSvc.createDatabase(m_dbId);
    } else {
      std::string temp = "COOL Database doesn't exist: " + m_dbId + " -- " + std::string(e.what());
      hlt2cool::COOL_Issue issue(ERS_HERE, temp.c_str());
      ers::error(issue);
      throw e;
    }
  } catch(coral::ConnectionServiceException& e) {
    std::string temp = "CORAL Connection service exception: " + std::string(e.what());
    hlt2cool::COOL_Issue issue(ERS_HERE, temp.c_str());
    ers::error(issue);
    throw e;
  } catch (std::exception &e) {
    std::string temp = "Unknown exception: " + std::string(e.what());
    hlt2cool::COOL_Issue issue(ERS_HERE, temp.c_str());
    ers::error(issue);
    throw e;
  }
}

void hlt2cool::HLTRatesCoolUtils::openFolder() {
  cool::PayloadMode::Mode payloadMode;
  // Vector payload have special payloadMode(VECTORPAYLOAD)
  // Blob and channels share the same PayloadMode(INLINEPAYLOAD)
  payloadMode = cool::PayloadMode::INLINEPAYLOAD;

  try {
    m_ratesfolder = m_dbPtr->getFolder(m_ratesfolderName);
  } catch ( cool::FolderNotFound & e) {
    // Create a new database if it's a sqlite file, not otherwise!
    if (m_dbId.find("sqlite:") != std::string::npos) {
      std::string temp = "Folder \"" + m_ratesfolderName + "\" does not exist. Trying to create it";
      hlt2cool::COOL_Issue issue(ERS_HERE, temp.c_str());
      ers::warning(issue);
      cool::FolderSpecification folderSpec(cool::FolderVersioning::MULTI_VERSION, m_recordSpec, payloadMode);
      m_ratesfolder = m_dbPtr->createFolder(m_ratesfolderName, folderSpec, m_folderDesc_time, true);
      ERS_INFO("Successfully created folder: " << m_ratesfolderName);
    } else {  // Folder doesn't exist in COOLONL
      std::string temp = "COOL Folder: " + m_ratesfolderName + " doesn't exist, HLT Rates will not be written to COOL";
      hlt2cool::COOL_Issue issue(ERS_HERE, temp.c_str());
      ers::error(issue);
      throw e;
    }
  } catch (std::exception &e) {
    std::string temp = "Unknown exception while opening rates folder: " + std::string(e.what());
    hlt2cool::COOL_Issue issue(ERS_HERE, temp.c_str());
    ers::fatal(issue);
    throw e;
  }

  try {
    m_chainsfolder = m_dbPtr->getFolder(m_chainsfolderName);
  } catch ( cool::FolderNotFound & e) {
    // Create a new database if it's a sqlite file
    if (m_dbId.find("sqlite:") != std::string::npos) {
      std::string temp = "Folder \"" + m_chainsfolderName + "\" does not exist. Trying to create it";
      hlt2cool::COOL_Issue issue(ERS_HERE, temp.c_str());
      ers::warning(issue);
      cool::FolderSpecification folderSpec(cool::FolderVersioning::MULTI_VERSION, m_chainrecordSpec, cool::PayloadMode::INLINEPAYLOAD);
      m_chainsfolder = m_dbPtr->createFolder(m_chainsfolderName, folderSpec, m_folderDesc_runlumi, true);
      ERS_INFO("Successfully created folder" << m_chainsfolderName);
    } else {  // Folder doesn't exist in COOLONL
      std::string temp = "COOL Folder: " + m_chainsfolderName + " doesn't exist, HLT Rates will not be written to COOL";
      hlt2cool::COOL_Issue issue(ERS_HERE, temp.c_str());
      ers::error(issue);
      throw e;
    }
  } catch (std::exception &e) {
    std::string temp = "Unknown exception while opening chain folder: " + std::string(e.what());
    hlt2cool::COOL_Issue issue(ERS_HERE, temp.c_str());
    ers::fatal(issue);
    throw e;
  }

  if (!m_ratesfolder || !m_chainsfolder) {
    hlt2cool::COOL_Issue issue(ERS_HERE, "COOL Folder not found!");
    ers::error(issue);
    exit(0);
  }
}

void hlt2cool::HLTRatesCoolUtils::setRecordSpec() {
  m_chainrecordSpec.extend("chains", cool::StorageType::String16M);
  m_recordSpec.extend("rates", cool::StorageType::Blob16M);
}

void hlt2cool::HLTRatesCoolUtils::writeChains(const std::vector<std::string> & vchains, const uint64_t runnumber) {
  openDatabase();
  openFolder();
  unsigned int lumiBlockNumber = 0;
  const cool::ValidityKey validsince = ((runnumber << 32) | lumiBlockNumber);
  const cool::ValidityKey validuntil = (((runnumber+1) << 32) | lumiBlockNumber);

  // If until = 0, write to an open ended IOV (until maximum validtykey)
  bool userTagOnly = true;  // Otherwise 2 rows written for each insert, one to tag, one to global head. I think we should keep it 1

  cool::Record record(m_chainrecordSpec);

  // Create a comma seperated list of all chain names
  std::string chainstring = std::accumulate(std::next(vchains.begin()), vchains.end(),
                                  vchains[0],  // start with first element
                                  [](std::string a, std::string b) {
                                  return a + ',' + b; });
  record["chains"].setValue<cool::String16M>(chainstring);
  unsigned channel = 0;
  m_chainsfolder->storeObject(validsince, validuntil, record, channel , m_chainstag, userTagOnly);
  m_dbPtr->closeDatabase();
}

void hlt2cool::HLTRatesCoolUtils::writeRates(const std::vector<float> & data, const uint64_t since, const uint64_t until) {
  // Leave exception handling to HLTRates2Cool.
  openDatabase();
  openFolder();
  cool::ValidityKey validuntil = until;

  // If until = 0, write to an open ended IOV (until maximum validtykey)
  if (until == 0) {
    validuntil = cool::ValidityKeyMax;
  }

  cool::ValidityKey validsince = since;

  bool userTagOnly = true;  // Otherwise 2 rows written for each insert, one to tag, one to global head. I think we should keep it 1

  cool::Record record(m_recordSpec);

  cool::IRecordVector vcool;

  // Needed for blob
  const float * pdata = &data[0];

  cool::Blob16M blob;

  // Pointer to vectors first element
  ERS_LOG("Writing all chain rates as a blob");
  blob.resize(data.size() * sizeof(float));
  //std::cout << "Starting address: " << static_cast<unsigned*>(blob.startingAddress()) << std::endl;
  ERS_DEBUG(1, "resized blob");
  std::memcpy(blob.startingAddress(), pdata, data.size() * sizeof(float));
  ERS_LOG("copied memory: " << data.size() * sizeof(float) << " bytes");
  record["rates"].setValue<cool::Blob16M>(blob);
  ERS_DEBUG(1, "Set record");
  m_ratesfolder->storeObject(validsince, validuntil, record, 0, m_ratestag, userTagOnly);
  ERS_DEBUG(1, "Wrote all chains as blob");
  ERS_DEBUG(1, "Data:" << pdata[0] << "," << pdata[1] << "," << pdata[2] << " ...");

  m_dbPtr->closeDatabase();
}

hlt2cool::HLTRatesCoolUtils::~HLTRatesCoolUtils() {
  m_dbPtr->closeDatabase();
}
